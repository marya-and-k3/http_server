#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
  Author:   --<alexandr69@gmail.com>
  Purpose:
  Created: 03/04/21
"""

import requests

import os, sys
tpath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, tpath)

from logger import get_logger

from config_progect import mconfig

logger = get_logger(__name__)


SPEC_STOP = 'stop_server'

SPEC = '''<?xml version="1.0" encoding="utf-8"?>
<CalculationDto xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <CADParams />
  <IssueDiscountList />
  <Items>
    <CalculationItemDto>
      <Id>700a9141-9506-4017-8cae-49bc3fa11b63</Id>
      <SourceType>TriCAD</SourceType>
      <Nst>false</Nst>
      <SortOrder>0</SortOrder>
      <WareId>0</WareId>
      <ModelId>0</ModelId>
      <WareName>Шкаф N04-500</WareName>
      <WareParams />
      <DwgParams />
      <NstParams />
      <CatalogParams />
      <CADParams>
        <Lookup>
          <Key>L</Key>
          <KeyName>Длина в 3cad (мм)</KeyName>
          <Value xsi:type="xsd:decimal">500</Value>
        </Lookup>
        <Lookup>
          <Key>A</Key>
          <KeyName>Высота в 3cad (мм)</KeyName>
          <Value xsi:type="xsd:decimal">360</Value>
        </Lookup>
        <Lookup>
          <Key>P</Key>
          <KeyName>Ширина в 3cad (мм)</KeyName>
          <Value xsi:type="xsd:decimal">315</Value>
        </Lookup>
        <Lookup>
          <Key>NPR</Key>
          <KeyName>Номер прототипа</KeyName>
          <Value xsi:type="xsd:string">N04</Value>
        </Lookup>
        <Lookup>
          <Key>CLR</Key>
          <KeyName>Цвет ДСП</KeyName>
          <Value xsi:type="xsd:string">RED01</Value>
        </Lookup>
      </CADParams>
      <Unit>Count</Unit>
      <Qty>1</Qty>
      <QtyByP>0</QtyByP>
      <Delta>0</Delta>
      <CalculationItemType>0</CalculationItemType>
      <PriceKind>WithPricePlus</PriceKind>
      <Notes />
      <Total>0</Total>
      <TotalWithSale>0</TotalWithSale>
      <SalePtc>0</SalePtc>
      <LastChangeDate xsi:nil="true" />
      <IssueDiscountList />
      <BillParams />
      <B2CSales />
      <Discounts />
      <NSTTypes />
      <Works />
    </CalculationItemDto>
    </Items>
  <PriceDate>2021-03-02T00:00:00+04:00</PriceDate>
  <MR3ShopId>0</MR3ShopId>
  <CatalogId>0</CatalogId>
  <PriceListId>0</PriceListId>
  <PriceZoneId>0</PriceZoneId>
  <Discounts />
  <Actions />
  <BillParams />
</CalculationDto>
'''
def readfileexample():
    href = os.path.join(mconfig['Example']['Path'],mconfig['Example']['File']) #r'c:\Users\a-dragunkin\REPO\http_server\example\t\spec-soap.xml'
    f = open(href, 'rb')
    data = f.read()
    f.close()
    return data

def main(data):

    adress = mconfig['DEFAULT']['adress']
    port = int(mconfig['DEFAULT']['port'])
    try :
        response = requests.post(f"http://{adress}:{port}/esp", data)
        if response.status_code == 200:
            response.encoding = 'utf-8'
            esp_k3 = response.text
            logger.info(esp_k3)
        elif response.status_code == 500:
            response.encoding = 'utf-8'
            logger.info('Сервер вернул код 500')
    except requests.exceptions.ConnectionError as err:
        logger.error(f'Cервер http://{adress}:{port} не доступен!!!\n {err}')


if __name__ == '__main__':
    main(readfileexample())
    #main(SPEC.encode())
    # main(SPEC_STOP.encode())
