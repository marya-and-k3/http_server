# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------------
# Name:        logger
# Purpose:     Вывод отладочных сообщений в лог файл
#
# Author:       Александр Драгункин --<alexandr69@gmail.com>
#
# Created:     20.08.2018....2020
#
# Copyright:
# Licence:
# -------------------------------------------------------------------------------

import sys
import os


import logging
import logging.config
import logging.handlers
from logging.handlers import RotatingFileHandler

#from mMpathexpand import Mpathexpand as mpex
#import configparser

from config_progect import mconfig

LOGEER_SECTION_NAME = 'LogClient'

# Имя логфайла
LOGFILENAME = mconfig[LOGEER_SECTION_NAME]['addfilename'] # 'cnc_dev.log'

# путь к папке
currpath = os.path.dirname(os.path.abspath(__file__))
APPDATA = os.getenv('APPDATA')
DIRLOGS = os.path.join(APPDATA, 'LOGS_K3_SRV')

# Ограничим размер до 1048576  Байт ~ 1 МБ.
MAX_BYTES = 1048576
# число копий логфайла
BACKUP_COUNT = 5

# именованные уровни отладки
LEVELS = {'-INFO': logging.INFO,
          '-DEBUG': logging.DEBUG,
          '-WARNING': logging.WARNING,
          '-ERROR': logging.ERROR,
          '-CRITICAL': logging.CRITICAL}

# регистратор инициализированных logger
LOGGERS = {}

_log_format = f"%(asctime)s - [%(levelname)s] - %(name)s.%(funcName)s(%(lineno)d) - %(message)s"

_log_f_format = f"%(asctime)s | %(levelname)-8s | %(lineno)04d | %(message)s"


#def get_config():
    #return mconfig


#mconfig = get_config()


def check_folder(path):
    try:
        if not os.path.exists(path):
            os.mkdir(path)
        return True
    except:
        return False


def create_path_logdir():
    if check_folder(DIRLOGS):
        path = os.path.join(DIRLOGS, LOGFILENAME)
    else:
        raise ValueError(f'ОТсутствует папка {DIRLOGS}')
    return path


def get_file_handler():
    path = create_path_logdir()
    file_handler = RotatingFileHandler(
        path, maxBytes=MAX_BYTES, backupCount=BACKUP_COUNT)
    file_handler = logging.FileHandler(path)
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(logging.Formatter(_log_format))
    return file_handler


def get_stream_handler():
    stream_handler = logging.StreamHandler(sys.stdout)
    stream_handler.setLevel(logging.ERROR)
    stream_handler.setFormatter(logging.Formatter(_log_format))
    return stream_handler


def get_logger(name='root'):
    if LOGGERS.get(name):
        return LOGGERS.get(name)
    else:
        logger = logging.getLogger(name)
        logger_setlevel(logger)
        # while logger.handlers:
        # logger.removeHandler(logger.handlers[0])
        logger.addHandler(get_file_handler())
        logger.addHandler(get_stream_handler())
        fixed_disable_existing_loggers()
        LOGGERS[name] = logger
        return logger

def logger_setlevel(logger):
    try:
        logger.setLevel(LEVELS[mconfig[LOGEER_SECTION_NAME]['level']])
    except:
        logger.setLevel(logging.ERROR)


# меням disabled на property что бы увидеть кто его изменяет
def get_disabled(self):
    return self._disabled


def set_disabled(self, disabled):
    frame = sys._getframe(1)
    if disabled:
        print('{}:{} disabled the {} logger'.format(
            frame.f_code.co_filename, frame.f_lineno, self.name))
    self._disabled = disabled

# раскоментарить что бы видеть
# logging.Logger.disabled = property(get_disabled, set_disabled)
# ---------------------------------------------------------------------


def fixed_disable_existing_loggers():
    """Отмените неприятное значение по умолчанию, и теперь существующие и новые регистраторы снова работают."""
    logging.config.dictConfig({
        'version': 1,
        'disable_existing_loggers': False,
    })


# -------------------------------------------------------------------

