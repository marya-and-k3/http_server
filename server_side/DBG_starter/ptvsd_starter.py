# -*- coding: utf-8 -*-
import sys
import addFolderToSysPath


from user_build.logger import (get_logger,)

logger = get_logger(__name__)
try:
    import ptvsd
except ImportError:
    logger.error(
        'ptvsd не импортирован. Отладка не возможна. Проверьте доступность ptvsd для импорта')
    for path in sys.path:
        logger.error(path)

try:
    import pydevd
except ImportError:
    logger.error(
        'pydevd не импортирован. Отладка не возможна. Проверьте доступность pydevd для импорта')
    for path in sys.path:
        print(path)


def start_vc():
    """
    Start the debugging server for VC Code
    """
    logger.debug('Запуск pydevd')
    try:
        number_attempts = 0
        total_attempts = 6
        while not ptvsd.is_attached() and number_attempts < total_attempts:
            number_attempts += 1
            logger.debug(
                f'Включите отладчик VS Code в активном сеансе {number_attempts}')
            ptvsd.enable_attach()
            # time.sleep(1)
            ptvsd.wait_for_attach(5)
    except:
        logger.error('ошибка is_attached')

    if not ptvsd.is_attached():
        logger.debug('отладчик VS Code в активном сеансе не был включен')
        return
    logger.debug('отладчик VS Code в активном сеансе включен')
    pydevd_settrace()


def pydevd_settrace():
    logger.debug('pydevd стартуем')
    if ptvsd.is_attached():
        logger.debug('ptvsd подключен')
        pydevd.settrace('localhost', port=5678, stdoutToServer=True, stderrToServer=True)
        # logger.debug('pydevd запущен')
