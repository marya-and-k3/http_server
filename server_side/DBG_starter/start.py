# -*- coding: utf-8 -*-
import sys
for p in sys.path:
    print(p)

from . import winproc
from . import ptvsd_starter
from user_build.logger import (get_logger,)

logger = get_logger(__name__)
try:
    list_name_proc = [proc.name for proc in winproc.get_process_list()]
    if 'Code.exe' in list_name_proc:
        logger.debug('Подключаем отладчик VS Code')
        ptvsd_starter.start_vc()
    else:
        try:
            logger.debug('Подключаем отладчик Wing IDE')
            import wingdbstub
            logger.debug('Подключен отладчик Wing IDE')
        except ImportError:
            logger.error('Ошибка импорта wingdbstub')
except:
    logger.error('ошибка запуска отладчика')