#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
  Author:  Aleksandr Dragunkin --<alexandr69@gmail.com> 
  Purpose: Получить список процессов Windows, например, для отслеживания какого либо приложения
  Created: "2021/01/13, 08:31:09"
  
  example:
   import winproc
   process_list = winproc.get_process_list()
   
   import winproc
   process_list = winproc.get_process_list()
   for process in process_list:
       if process.name == 'notepad.exe':
           print(process.name, process.id, process.working_set_size)
           process.kill()
"""

import subprocess


class Process:
    """
    Класс процесса для удобства дальнейшей работы с процессами.
    """
    def __init__(self, handle_count, name, priority, process_id, thread_count, working_set_size):
        #  принять параметры процесса и присвоить атрибутам класса их значения
        self.handle_count = handle_count
        self.name = name
        self.priority = priority
        self.id = process_id
        self.thread_count = thread_count
        self.working_set_size = working_set_size
        
    def kill(self) -> bool:
        """
        Вызывает знакомый всем Taskkill и убивает процесс с PID, который записан
        в self.id. Если процесс убит, то функция вернет True, если же нет, то False.
        
        (Убийство процессов будет работать корректно, только если скрипт
        запущен с правами администратора)
        """
        try:
            subprocess.check_output('Taskkill /PID {} /F /T'.format(self.id), shell=True)
            return True
        except:
            return False
        
        
def get_process_list() -> list:
    """
    Получить список процессов
    """
    output = str(subprocess.check_output('wmic process list brief /format:list', shell=True).strip())
    process_list_as_strings = output.split('\\r\\r\\n\\r\\r\\n\\r\\r\\n')
    process_list_as_process = []
    exception_patterns = ['\'', 'b', '=', 'HandleCount', 'Name', 'Priority',
                          'ProcessId', 'ThreadCount', 'WorkingSetSize']
    for process_string in process_list_as_strings:
        for exception_pattern in exception_patterns:
            process_string = process_string.replace(exception_pattern, '')
        process_attributes = process_string.split('\\r\\r\\n')
        if len(process_attributes) == 6:
            process = Process(*process_attributes)
            process_list_as_process.append(process)
        else:
            print('Warning: Не удается обработать один из процессов')
    return process_list_as_process



def main():
    fix_size = 55
    for proc in get_process_list():
        len_info = len(f"{proc.name}{proc.id}")
        dop_s = '.' * (fix_size - len_info)
        print(f"{proc.name}{dop_s}{proc.id}")

if __name__ == '__main__':
    main()