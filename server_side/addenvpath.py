﻿# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------------
# Name:        addFolderToSysPath
# Purpose:     Модуль добавляет в SysPAth пути
#
# Author:      Aleksandr Dragunkin
#
# Created:     26.03.2021
# Copyright:   (c) GEOS 2012 http://k3info.ru/
# Licence:     FREE
# -------------------------------------------------------------------------------
__version__ = '1.0.0'
try:
    import k3
except ImportError:
    class k3:
        d_ident={
            "<app>":r'c:\PKM_CG8\Bin_Beta',
            "<proto>":r'c:\PKM_CG8\Data\PKM\Proto',
        }

        @classmethod
        def mpathexpand(cls, identifier):
            return cls.d_ident[identifier]

import sys
import os


gpath = os.path.dirname(os.path.abspath(__file__))

# Стандартный юзерский путь для поиска пакетов и модулей

def add_syspath(dvsyspath=gpath):

    for tpath in set(dvsyspath):
        if os.path.isdir(tpath):
            if tpath not in sys.path:
                sys.path.insert(0, tpath)


dvsyspath = [os.path.join(gpath, a) for a in
                [
                 r'site-packages',
                 ]
            ]
add_syspath(dvsyspath=dvsyspath)



