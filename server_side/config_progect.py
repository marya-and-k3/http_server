#!/usr/bin/env python3
#coding:utf-8
"""
  Author:  Aleksandr Dragunkin --<alexandr69@gmail.com>
  Purpose:
  Created: 03/25/21
"""

import sys
import os
import configparser

SETTINGSINI_NAME = 'project_settings.ini'
DIR_CONFIG = os.path.dirname(os.path.abspath(__file__))


def get_config(dir_config: str, settingsini_name: str, *checkers) -> configparser.ConfigParser:
    """прочитать параметры конфигурации

    args:
     - dir_config: str - путь к папке
     - settingsini_name: str - имя файла
    """
    full_name_cfg = os.path.join(dir_config, settingsini_name)
    config = configparser.ConfigParser()

    # проверить наличие файла
    check_ini_file(full_name_cfg, config)
    with open(full_name_cfg) as fp:
        config.read_file(fp)

    for check in checkers:
        check(full_name_cfg, config)

    config.read(full_name_cfg)
    return config

def check_log_server_section(full_name_cfg: str, config: configparser.ConfigParser) -> bool:
    """проверить наличие секции LogServer"""
    if not config.has_section('LogServer'):
        config['LogServer'] = {}
        config['LogServer']['addfilename']='serverk3.log'
        config['LogServer']['level']='-DEBUG'
        with open(full_name_cfg, '+w') as fp:
            config.write(fp)
    return True

def check_log_client_section(full_name_cfg: str, config: configparser.ConfigParser) -> bool:
    """проверить наличие секции LogClient"""
    if not config.has_section('LogClient'):
        config['LogClient'] = {}
        config['LogClient']['addfilename']='clientk3.log'
        config['LogClient']['level']='-DEBUG'
        with open(full_name_cfg, '+w') as fp:
            config.write(fp)
    return True

def check_ini_file(full_name_cfg: str, config: configparser.ConfigParser) -> bool:
    """проверить наличие ini-файла по полному имени full_name_cfg

    Если такого файла нет создать и заполнить структуру по умолчанию
    """
    is_inifile = os.path.isfile(full_name_cfg)
    if not is_inifile:
        config['DEFAULT'] = {'Adress': '127.0.0.1',
                             'Port': 9009,}
        with open(full_name_cfg, 'w') as configfile:
            config.write(configfile)
    return True


mconfig = get_config(DIR_CONFIG, SETTINGSINI_NAME, check_log_server_section, check_log_client_section)