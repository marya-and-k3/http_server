#!/usr/bin/env python3
#coding:utf-8
"""
  Author:   --<>
  Purpose:
  Created: 03/26/21
"""
import time
import json
from functools import lru_cache
from typing import Dict,List,Tuple,Any
import k3

from dataclasses import dataclass

from logger import get_logger
logger = get_logger(__name__)


# Ниже текст коорый должен быть в самом модуле
# эти функции связаны с __file__

DEFAULT_NAME_PROTO = "N04"
DEFAULT_NAME_LIB = "MKitchen"


def _get_bibl_name():
    """Имя папки и библиотеки прототипа MKitchen"""
    return DEFAULT_NAME_LIB


def _get_proto_id(name_proto:str)->int:
    proto_id=k3.getprotoid(_get_bibl_name(), name_proto, "ProtoName", name_proto)
    return int(proto_id)

def _test_protoobj_creator():
    proto_id=_get_proto_id('N04')
    _params=[k3.k_create, f'{_get_bibl_name()}.ptl', int(proto_id),
    'h', 700,
    'w', 300,
    'd', 500,
    k3.k_done,0,0,0]
    print(_params)
    k3.protoobj(_params)

# ------------------

@dataclass
class CADParam:
    key:str
    key_name:str
    value:any

@dataclass
class CADParams:
    _id:str
    params:Dict[str,Tuple[str,Any]]

def clear_scene()->None:
    if k3.sysvar(60):
        k3.delete(k3.k_all)
        logger.info('очистили сцену')
        print(k3.sysvar(60))

def source_to_dict(cid:object)->Dict[str,Any]:
    try:
        _s=cid.Source
        return json.loads(_s)
    except:
        logger.exception('ошибка конвертации cid.Source')


def cad_params_creator(cid:object)->CADParams:
    logger.debug(f'ID = {cid.Id}')
    cad_params=CADParams(cid.Id,dict())
    for cp in cid.CADParams.Lookup:
        logger.debug(f'{cp.Key}, {cp.KeyName}, {cp.Value.gds_elementtree_node_.text}')
        cad_params.params[cp.Key]=(cp.KeyName, cp.Value.gds_elementtree_node_.text)
    source_dict=source_to_dict(cid)

    return cad_params

@dataclass
class PositionLastPredmet:
    x:float
    y:float
    z:float

position_last_predmet=PositionLastPredmet(0,0,0)

@lru_cache()
def get_id_color_karkase(param:float)->float:
    """Определить id материала(ДСП) каркаса по свойству цвет из справочника mr3

    Args:
        param (float): ID_value_par_mr3 id цвета каркаса из справочника mr3

    Returns:
        float: id материала(ДСП) каркаса из области применния Материал деталей
    """
    from core_k import yadtools
    id_color=0
    n=k3.npgetbywhere(1,f'[ID_value_par_mr3]={param}','arr',yadtools.getnumgroup(2))
    if n:
        a=k3.VarArray(int(n),'arr')
        id_color=a[0].value
        logger.debug(f"для цвета {param} подобран {id_color} {k3.priceinfo(id_color,'MatName','NoName')}")
    return id_color

@lru_cache()
def get_id_color_band_karkase(param:float)->float:
    """Определить id кромки(ДСП) каркаса по свойству приоритетный цвет кромки(BandColor)
    для материала кркаса

    Args:
        param (float): ID_value_par_mr3 id цвета каркаса  из справочника mr3

    Returns:
        float: id кромки(ДСП) каркаса из области применния Типы кромки
    """
    id_color=0
    id_mat_carkase = get_id_color_karkase(param)
    id_color=k3.priceinfo(id_mat_carkase,'BandColor',0)
    if id_color:
        logger.debug(f"для цвета ДСП {param} подобрана кромка {id_color} {k3.priceinfo(id_color,'MatName','NoName')}")
    else:
        logger.error(f'У материала {id_mat_carkase} отсутствует свойство BandColor')
    return id_color

def protoobj_creator(cid:object)->k3.K3Obj:
    """[summary]

    Args:
        cid (object): [description]

    Returns:
        k3.K3Obj: [description]
    """
    obj=None
    cad_params=cad_params_creator(cid)
    if 'NPR' in cad_params.params:
        name_proto=cad_params.params['NPR'][1]
        proto_id=_get_proto_id(name_proto)
        if proto_id>0:
            _params=[k3.k_create, f'{_get_bibl_name()}.ptl', proto_id,
            'h', float(cad_params.params['A'][1]),
            'w', float(cad_params.params['L'][1]),
            'd', float(cad_params.params['P'][1]),
            'CK', get_id_color_karkase(float(cad_params.params['CK'][1])),
            'CKR',get_id_color_band_karkase(float(cad_params.params['CK'][1])),
            k3.k_done,0,0,0]
            logger.debug(_params)

            k3.protoobj(_params)
            k3.vifilter(k3.k_default)
            # k3.smart(k3.k_edit,k3.k_list,k3.k_last,1)
            _obj=k3.Var()
            k3.objident(k3.k_last,1,_obj)
            obj=_obj.value
            k3.move(obj,k3.k_done,position_last_predmet.x, position_last_predmet.y, position_last_predmet.z)
            position_last_predmet.x+=float(cad_params.params['L'][1])
            k3.zoom(k3.k_all)
            # k3.interact()
        else:
            logger.error(f'{name_proto} не реализован.')
            obj=None
    elif hasattr(cid,'ParentId'):
        # Судя по всему это фасад для предыдущего предмета
        obj=None
    return obj

def calculate(height = 0, width = 0, length = 0, count = 1) -> str:
    sr = f'''<?xml version="1.0" encoding="utf-8"?>
<CalculationDto xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <CADParams/>
    <IssueDiscountList/>
    <Items>
        <CalculationItemDto>
            <Id>ccf94219-3a7a-4471-a9bb-76764eb5ce16</Id>
            <SourceType>TriCadNext</SourceType>
            <Source></Source>
            <Nst>false</Nst>
            <SortOrder>0</SortOrder>
            <WareId>0</WareId>
            <ModelId>0</ModelId>
            <WareName>Нижние с одним распашным фасадом</WareName>
            <WareParams/>
            <DwgParams/>
            <NstParams/>
            <CatalogParams/>
            <CADParams>
                <Lookup>
                    <Key>COD</Key>
                    <KeyName>Полный артикул товара в 3cad</KeyName>
                    <Value xsi:type="xsd:string">MA0K5704-00-0300G-P</Value>
                </Lookup>
                <Lookup>
                    <Key>L</Key>
                    <KeyName>Длина в 3cad (мм)</KeyName>
                    <Value xsi:type="xsd:decimal">300</Value>
                </Lookup>
                <Lookup>
                    <Key>A</Key>
                    <KeyName>Высота в 3cad (мм)</KeyName>
                    <Value xsi:type="xsd:decimal">1320</Value>
                </Lookup>
                <Lookup>
                    <Key>P</Key>
                    <KeyName>Ширина в 3cad (мм)</KeyName>
                    <Value xsi:type="xsd:decimal">560</Value>
                </Lookup>
                <Lookup>
                    <Key>MP</Key>
                    <Value xsi:type="xsd:string">MA0</Value>
                </Lookup>
                <Lookup>
                    <Key>NPR</Key>
                    <Value xsi:type="xsd:string">N04</Value>
                </Lookup>
                <Lookup>
                    <Key>PBL</Key>
                    <Value xsi:type="xsd:string">560</Value>
                </Lookup>
                <Lookup>
                    <Key>PBP</Key>
                    <Value xsi:type="xsd:string">560</Value>
                </Lookup>
                <Lookup>
                    <Key>PK</Key>
                    <Value xsi:type="xsd:string">560</Value>
                </Lookup>
                <Lookup>
                    <Key>S_N</Key>
                    <Value xsi:type="xsd:string">1</Value>
                </Lookup>
                <Lookup>
                    <Key>KOV</Key>
                    <Value xsi:type="xsd:string">0</Value>
                </Lookup>
                <Lookup>
                    <Key>FBL</Key>
                    <Value xsi:type="xsd:string">0</Value>
                </Lookup>
                <Lookup>
                    <Key>FBP</Key>
                    <Value xsi:type="xsd:string">0</Value>
                </Lookup>
                <Lookup>
                    <Key>POLK</Key>
                    <Value xsi:type="xsd:string">1</Value>
                </Lookup>
                <Lookup>
                    <Key>NP</Key>
                    <Value xsi:type="xsd:string">3</Value>
                </Lookup>
                <Lookup>
                    <Key>RP</Key>
                    <Value xsi:type="xsd:string">2</Value>
                </Lookup>
                <Lookup>
                    <Key>KP1</Key>
                    <Value xsi:type="xsd:string">360</Value>
                </Lookup>
                <Lookup>
                    <Key>KP2</Key>
                    <Value xsi:type="xsd:string">720</Value>
                </Lookup>
                <Lookup>
                    <Key>KP3</Key>
                    <Value xsi:type="xsd:string">1020</Value>
                </Lookup>
                <Lookup>
                    <Key>SZS</Key>
                    <Value xsi:type="xsd:string">17</Value>
                </Lookup>
                <Lookup>
                    <Key>NOG</Key>
                    <Value xsi:type="xsd:string">1</Value>
                </Lookup>
                <Lookup>
                    <Key>CK</Key>
                    <Value xsi:type="xsd:string">317532506</Value>
                </Lookup>
                <Lookup>
                    <Key>KREP</Key>
                    <Value xsi:type="xsd:string">1</Value>
                </Lookup>
                <Lookup>
                    <Key>CP</Key>
                    <Value xsi:type="xsd:string">1</Value>
                </Lookup>
                <Lookup>
                    <Key>TP</Key>
                    <Value xsi:type="xsd:string">40813273</Value>
                </Lookup>
                <Lookup>
                    <Key>VPR</Key>
                    <Value xsi:type="xsd:string">1</Value>
                </Lookup>
            </CADParams>
            <Unit>Count</Unit>
            <Qty>0</Qty>
            <QtyByP>0</QtyByP>
            <Delta>0</Delta>
            <CalculationItemType>0</CalculationItemType>
            <PriceKind>WithPricePlus</PriceKind>
            <Notes/>
            <Total>0</Total>
            <TotalWithSale>0</TotalWithSale>
            <SalePtc>0</SalePtc>
            <LastChangeDate xsi:nil="true"/>
            <IssueDiscountList/>
            <BillParams/>
            <B2CSales/>
            <Discounts/>
            <NSTTypes/>
            <Works/>
        </CalculationItemDto>
        <CalculationItemDto>
            <Id>fcbde5a6-b738-4796-bb31-9602c12236f3</Id>
            <ParentId>ccf94219-3a7a-4471-a9bb-76764eb5ce16</ParentId>
            <SourceType>TriCadNext</SourceType>
            <Source></Source>
            <Nst>false</Nst>
            <SortOrder>0</SortOrder>
            <WareId>0</WareId>
            <ModelId>0</ModelId>
            <WareName>* missing *</WareName>
            <WareParams/>
            <DwgParams/>
            <NstParams/>
            <CatalogParams/>
            <CADParams>
                <Lookup>
                    <Key>COD</Key>
                    <KeyName>Полный артикул товара в 3cad</KeyName>
                    <Value xsi:type="xsd:string">MA0G1316X296</Value>
                </Lookup>
                <Lookup>
                    <Key>L</Key>
                    <KeyName>Длина в 3cad (мм)</KeyName>
                    <Value xsi:type="xsd:decimal">298</Value>
                </Lookup>
                <Lookup>
                    <Key>A</Key>
                    <KeyName>Высота в 3cad (мм)</KeyName>
                    <Value xsi:type="xsd:decimal">1316</Value>
                </Lookup>
                <Lookup>
                    <Key>P</Key>
                    <KeyName>Ширина в 3cad (мм)</KeyName>
                    <Value xsi:type="xsd:decimal">22</Value>
                </Lookup>
                <Lookup>
                    <Key>MP</Key>
                    <Value xsi:type="xsd:string">MA0</Value>
                </Lookup>
                <Lookup>
                    <Key>TF</Key>
                    <Value xsi:type="xsd:string">G</Value>
                </Lookup>
                <Lookup>
                    <Key>NF</Key>
                    <Value xsi:type="xsd:string">F1</Value>
                </Lookup>
                <Lookup>
                    <Key>CF</Key>
                    <Value xsi:type="xsd:string">428342084</Value>
                </Lookup>
                <Lookup>
                    <Key>NT</Key>
                    <Value xsi:type="xsd:string">1</Value>
                </Lookup>
                <Lookup>
                    <Key>CKF</Key>
                    <Value xsi:type="xsd:string">428372087</Value>
                </Lookup>
            </CADParams>
            <Unit>Count</Unit>
            <Qty>0</Qty>
            <QtyByP>0</QtyByP>
            <Delta>0</Delta>
            <CalculationItemType>0</CalculationItemType>
            <PriceKind>WithPricePlus</PriceKind>
            <Notes/>
            <Total>0</Total>
            <TotalWithSale>0</TotalWithSale>
            <SalePtc>0</SalePtc>
            <LastChangeDate xsi:nil="true"/>
            <IssueDiscountList/>
            <BillParams/>
            <B2CSales/>
            <Discounts/>
            <NSTTypes/>
            <Works/>
        </CalculationItemDto>
        <CalculationItemDto>
            <Id>b0dd1acf-83b7-47fb-9f23-82f80e7f6cfc</Id>
            <SourceType>TriCadNext</SourceType>
            <Source></Source>
            <Nst>false</Nst>
            <SortOrder>0</SortOrder>
            <WareId>0</WareId>
            <ModelId>0</ModelId>
            <WareName>Нижние с одним распашным фасадом</WareName>
            <WareParams/>
            <DwgParams/>
            <NstParams/>
            <CatalogParams/>
            <CADParams>
                <Lookup>
                    <Key>COD</Key>
                    <KeyName>Полный артикул товара в 3cad</KeyName>
                    <Value xsi:type="xsd:string">MA0N5304-00-0300G-L</Value>
                </Lookup>
                <Lookup>
                    <Key>L</Key>
                    <KeyName>Длина в 3cad (мм)</KeyName>
                    <Value xsi:type="xsd:decimal">300</Value>
                </Lookup>
                <Lookup>
                    <Key>A</Key>
                    <KeyName>Высота в 3cad (мм)</KeyName>
                    <Value xsi:type="xsd:decimal">720</Value>
                </Lookup>
                <Lookup>
                    <Key>P</Key>
                    <KeyName>Ширина в 3cad (мм)</KeyName>
                    <Value xsi:type="xsd:decimal">560</Value>
                </Lookup>
                <Lookup>
                    <Key>MP</Key>
                    <Value xsi:type="xsd:string">MA0</Value>
                </Lookup>
                <Lookup>
                    <Key>NPR</Key>
                    <Value xsi:type="xsd:string">N04</Value>
                </Lookup>
                <Lookup>
                    <Key>PBL</Key>
                    <Value xsi:type="xsd:string">560</Value>
                </Lookup>
                <Lookup>
                    <Key>PBP</Key>
                    <Value xsi:type="xsd:string">560</Value>
                </Lookup>
                <Lookup>
                    <Key>PK</Key>
                    <Value xsi:type="xsd:string">560</Value>
                </Lookup>
                <Lookup>
                    <Key>S_N</Key>
                    <Value xsi:type="xsd:string">1</Value>
                </Lookup>
                <Lookup>
                    <Key>KOV</Key>
                    <Value xsi:type="xsd:string">0</Value>
                </Lookup>
                <Lookup>
                    <Key>FBL</Key>
                    <Value xsi:type="xsd:string">0</Value>
                </Lookup>
                <Lookup>
                    <Key>FBP</Key>
                    <Value xsi:type="xsd:string">0</Value>
                </Lookup>
                <Lookup>
                    <Key>POLK</Key>
                    <Value xsi:type="xsd:string">1</Value>
                </Lookup>
                <Lookup>
                    <Key>NP</Key>
                    <Value xsi:type="xsd:string">1</Value>
                </Lookup>
                <Lookup>
                    <Key>RP</Key>
                    <Value xsi:type="xsd:string">2</Value>
                </Lookup>
                <Lookup>
                    <Key>KP1</Key>
                    <Value xsi:type="xsd:string">360</Value>
                </Lookup>
                <Lookup>
                    <Key>SZS</Key>
                    <Value xsi:type="xsd:string">17</Value>
                </Lookup>
                <Lookup>
                    <Key>NOG</Key>
                    <Value xsi:type="xsd:string">1</Value>
                </Lookup>
                <Lookup>
                    <Key>CK</Key>
                    <Value xsi:type="xsd:string">246797123</Value>
                </Lookup>
                <Lookup>
                    <Key>KREP</Key>
                    <Value xsi:type="xsd:string">1</Value>
                </Lookup>
                <Lookup>
                    <Key>CP</Key>
                    <Value xsi:type="xsd:string">1</Value>
                </Lookup>
                <Lookup>
                    <Key>TP</Key>
                    <Value xsi:type="xsd:string">40813273</Value>
                </Lookup>
                <Lookup>
                    <Key>VPR</Key>
                    <Value xsi:type="xsd:string">1</Value>
                </Lookup>
            </CADParams>
            <Unit>Count</Unit>
            <Qty>0</Qty>
            <QtyByP>0</QtyByP>
            <Delta>0</Delta>
            <CalculationItemType>0</CalculationItemType>
            <PriceKind>WithPricePlus</PriceKind>
            <Notes/>
            <Total>0</Total>
            <TotalWithSale>0</TotalWithSale>
            <SalePtc>0</SalePtc>
            <LastChangeDate xsi:nil="true"/>
            <IssueDiscountList/>
            <BillParams/>
            <B2CSales/>
            <Discounts/>
            <NSTTypes/>
            <Works/>
        </CalculationItemDto>
        <CalculationItemDto>
            <Id>3678338c-a2bf-48c4-9fb7-cd9f73224cd1</Id>
            <ParentId>b0dd1acf-83b7-47fb-9f23-82f80e7f6cfc</ParentId>
            <SourceType>TriCadNext</SourceType>
            <Source></Source>
            <Nst>false</Nst>
            <SortOrder>0</SortOrder>
            <WareId>0</WareId>
            <ModelId>0</ModelId>
            <WareName>* missing *</WareName>
            <WareParams/>
            <DwgParams/>
            <NstParams/>
            <CatalogParams/>
            <CADParams>
                <Lookup>
                    <Key>COD</Key>
                    <KeyName>Полный артикул товара в 3cad</KeyName>
                    <Value xsi:type="xsd:string">MA0G716X296</Value>
                </Lookup>
                <Lookup>
                    <Key>L</Key>
                    <KeyName>Длина в 3cad (мм)</KeyName>
                    <Value xsi:type="xsd:decimal">298</Value>
                </Lookup>
                <Lookup>
                    <Key>A</Key>
                    <KeyName>Высота в 3cad (мм)</KeyName>
                    <Value xsi:type="xsd:decimal">716</Value>
                </Lookup>
                <Lookup>
                    <Key>P</Key>
                    <KeyName>Ширина в 3cad (мм)</KeyName>
                    <Value xsi:type="xsd:decimal">22</Value>
                </Lookup>
                <Lookup>
                    <Key>MP</Key>
                    <Value xsi:type="xsd:string">MA0</Value>
                </Lookup>
                <Lookup>
                    <Key>TF</Key>
                    <Value xsi:type="xsd:string">G</Value>
                </Lookup>
                <Lookup>
                    <Key>NF</Key>
                    <Value xsi:type="xsd:string">F1</Value>
                </Lookup>
                <Lookup>
                    <Key>CF</Key>
                    <Value xsi:type="xsd:string">482342082</Value>
                </Lookup>
                <Lookup>
                    <Key>NT</Key>
                    <Value xsi:type="xsd:string">1</Value>
                </Lookup>
                <Lookup>
                    <Key>CKF</Key>
                    <Value xsi:type="xsd:string">428370784</Value>
                </Lookup>
            </CADParams>
            <Unit>Count</Unit>
            <Qty>0</Qty>
            <QtyByP>0</QtyByP>
            <Delta>0</Delta>
            <CalculationItemType>0</CalculationItemType>
            <PriceKind>WithPricePlus</PriceKind>
            <Notes/>
            <Total>0</Total>
            <TotalWithSale>0</TotalWithSale>
            <SalePtc>0</SalePtc>
            <LastChangeDate xsi:nil="true"/>
            <IssueDiscountList/>
            <BillParams/>
            <B2CSales/>
            <Discounts/>
            <NSTTypes/>
            <Works/>
        </CalculationItemDto>
    </Items>
    <PriceDate>0001-01-01T00:00:00</PriceDate>
    <MR3ShopId>0</MR3ShopId>
    <CatalogId>0</CatalogId>
    <PriceListId>0</PriceListId>
    <PriceZoneId>0</PriceZoneId>
    <Discounts/>
    <Actions/>
    <BillParams/>
</CalculationDto>
'''
    return sr

def interact():
    k3.interact()

