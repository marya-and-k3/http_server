# -*- coding: utf-8 -*-
#---------------------------------------------------------------------
# Name:        mMpathexpand
# Purpose:     Модуль работы с информацией системы
#
# Author:      Aleksandr Dragunkin
#
# Created:     25.09.2015
# Copyright:   (c) GEOS 2015 http://k3info.ru/
# Licence:     FREE
#----------------------------------------------------------------------
# import wingdbstub
import os, sys
from SingletonMetaClass import Singleton

try:
    import k3
except ImportError:
    class k3:
        # вычислить положение папки proto, site-packages
        # добавить sys.path
        gpath = os.path.dirname(os.path.abspath(__file__))
        plist = gpath.split('\\')
        plist.insert(1, os.sep)
        root_folder = os.path.join(*plist[:3])
        d_ident={
            "<app>":os.path.join(root_folder,'Bin'),
            "<proto>":os.path.join(root_folder,r'Data\PKM\Proto'),
            "<projects>":r"d:\PKMProjects80",
            "<appdata>":os.path.join(root_folder,r'Bin\App'),
            "<reports>":os.path.join(root_folder,r'Bin\Reports'),
            "<k3files>":os.path.join(root_folder,r'Data\PKM\k3files'),
            "<pictures>":os.path.join(root_folder,r'Data\PKM\pictures'),
            "<tests>":os.path.join(root_folder,r'Data\PKM\tests'),
        }

        @classmethod
        def mpathexpand(cls, identifier):
            return cls.d_ident[identifier.lower()]

class Mpathexpand(Singleton):
    """ Возвращает пути к служебным папкам
    PROTOPATH-- Папка с прототипами
    PROJECTS-- Папка проекта
    APPDATA-- Папка с пользовательскими установками журналом и прочей ...
    REPORTS -- Папка с отчетами
    K3FILESPATH-- Папка с моделями К3
    PICTURESPATH-- Папка с картинками
    TESTS-- Папка с тестами


    from mMpathexpand import Mpathexpand as mpex
    >>> mpex().PROTOPATH
    '....\\Data\\PKM\\Proto\\'
    >>> mpex().REPORTS
    '....\\BIN\\Reports\\'
    """
    PROTOPATH = k3.mpathexpand("<Proto>")
    PROJECTS = k3.mpathexpand("<PROJECTS>")
    APPDATA = k3.mpathexpand("<AppData>")
    REPORTS = k3.mpathexpand("<reports>")
    K3FILESPATH = k3.mpathexpand("<k3files>")
    PICTURESPATH = k3.mpathexpand("<pictures>")
    TESTS = k3.mpathexpand("<tests>")


if __name__ == '__main__':
    print(Mpathexpand.PROTOPATH)
    print(Mpathexpand.PROJECTS)
    print(Mpathexpand.APPDATA)
    print(Mpathexpand.REPORTS)
    print(Mpathexpand.K3FILESPATH)
    print(Mpathexpand.PICTURESPATH)
    print(Mpathexpand.TESTS)