#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Very simple HTTP server in python for logger requests
Usage::
    ./server.py [<port>]
"""
import time
from importlib import  reload
import os, sys
tpath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, tpath)

import addenvpath

# Для отладки
import DBG_starter
import DBG_starter.start
#

from dataclasses import dataclass
from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib.parse import urlparse

from config_progect import mconfig

from controller._controller import PATHS as paths_handlers

import k3_calculator

from logger import get_logger
logger = get_logger(__name__)

def hash_calc(value):
    import hashlib
    m = hashlib.new('md5')
    m.update(value)
    hhh = m.hexdigest()
    return hhh

@dataclass
class ConnectInfo:
    adress: str = None
    port: int = None

CONNECTINFO = ConnectInfo()

def wpap_localhost(adress):
    if adress == '0.0.0.0':
        return 'localhost'
    return adress

def stop_button():
    _d = {'adress': wpap_localhost(CONNECTINFO.adress),
          'port': CONNECTINFO.port,}
    return '''<style>
/* Стили кнопки */
.iksweb{{display: inline-block;cursor: pointer; font-size:14px;text-decoration:none;padding:10px 20px; color:#354251;background:#ffff0;border-radius:0px;border:2px solid #354251;}}
.iksweb:hover{{background:#354251;color:#ffffff;border:2px solid #354251;transition: all 0.2s ease;}}
</style>

<a class="iksweb" href="http://{adress}:{port}/exc?stop_server", target="_self"\
title="Stop Server">Stop Server</a>'''.format(**_d)

class DefaultHandlerHTTP(BaseHTTPRequestHandler):
    def _set_response(self, code = 200):
        self.send_response(code)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        logger.info(f"GET request,\nPath: {self.path}\nHeaders:\n{self.headers}\n")
        self._set_response()
        self.wfile.write("""<div>GET request for {}</div>
        <div>...</div>
        <div>{}</div>""".format(
            self.path, stop_button()).encode('utf-8'))

        url = urlparse(self.path) # .query
        path_arg = bytes(url.path.encode())
        command_arg = url.query.encode()
        # анализ команды выключения сервера
        self.close_server(path_arg, command_arg)

    def do_POST(self):
        #  Получает размер данных
        reload(k3_calculator)
        try:
            content_length = int(self.headers['Content-Length'])
            #  Получает сами данные
            post_data = self.rfile.read(content_length)
            #logger.info(f"""POST request,\nPath: {self.path}\n
            #Headers:\n{self.headers}\n\nBody:\n{post_data.decode('utf-8')}\n""")

            # уровень логики обрабатываем запрос
            #controller.__file__
            funct = paths_handlers.get(self.path, None)
            if funct:
                logger.info('\n<---' + 'POST' *5 + '--->\n')
                parse_obj = funct(post_data,silence=True)
                k3_calculator.clear_scene()
                for cid in parse_obj.Items.CalculationItemDto:
                    # logger.debug(f'ID = {cid.Id}')
                    k3_calculator.protoobj_creator(cid)
                    # for cp in cid.CADParams.Lookup:
                    #     logger.debug(f'{cp.Key}, {cp.KeyName}, {cp.Value}')
            # 1.Парсим XML
            # 2.Запускаем построение предмета по прототипу
            # 3.Формируем Базу выгрузки
            # 4.Формируем

            self._set_response()
            self.wfile.write("{}".format(k3_calculator.calculate(height = 360, width = 500, length = 315, count = 1)).encode('utf-8'))  # self.path
            k3_calculator.interact()
            k3_calculator.clear_scene()


            # анализ команды выключения сервера
            self.close_server(bytes(self.path.encode()), post_data)
        except:
            self._set_response(code = 500)
            logger.exception('Ошибка данных')


    def close_server(self, path_arg, command_arg):
        """остановить сервер"""
        if hash_calc(path_arg) == '33abcd655312adb3d2c4308f7b6ad38e':  # '/exc'
            if hash_calc(command_arg) == '3c437528e2c5f7857d57f37a7b6c88b6':  # 'stop_server'
                raise KeyboardInterrupt()

def run(server_class=HTTPServer, handler_class=DefaultHandlerHTTP, port=0):
    adress = mconfig["DEFAULT"]["adress"]
    port = int(mconfig["DEFAULT"]["port"])
    for _ in range(50):
        try:
            server_address = (adress, port)
            httpd = server_class(server_address, handler_class)
            break
        except OSError:
            port += 1
            logger.info(f"PORT " + str(port - 1) +
                         " недоступен. Пробуем ПОРТ " + str(port) )

    logger.info('\n\n<---' + 'START HTTP ' *5 + '--->\n')
    logger.info(f'Запуск HTTP SERVER  PORT {port}')
    try:
        CONNECTINFO.adress = adress
        CONNECTINFO.port = port
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
        logger.info(f'port ' + str(port - 1) +
                     ' взят. Пробный порт ' + str(port))

    httpd.server_close()
    logger.info('Остановка httpd...\n')
    close_mebel()


def close_mebel():
    """Закрыть приложение если оно запущено"""
    try:
        import k3
        k3.new(k3.k_clear)
        k3.quit()
    except ImportError:
        logger.error('...не смог закрыть к3. Отсутствует библиотека k3. По всей видимости mebel.exe уже закрыт или не был запущен')
    except k3.Error:
        logger.info('...закрываем к3.')
        k3.quit()
    finally:
        logger.info('Сеанс работы сервера завершён')


if __name__ == '__main__':
    from sys import argv

    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()
