#!/usr/bin/env python3
#coding:utf-8
"""
  Author:  Aleksandr Dragunkin --<alexandr69@gmail.com>
  Purpose:
  Created: 03/16/21
"""

import sys


import generator
import generator_subs

def test():
    #generator.supermod.CurrentSubclassModule_ = generator
    roota = generator.parse(r'd:\REPO\MARYA\http_server\example\spec-soap.xml', silence=True)
    generator_subs.supermod.CurrentSubclassModule_ = generator_subs
    rootb = generator_subs.parse(r'd:\REPO\MARYA\http_server\example\spec-soap.xml', silence=True)
    roota.export(sys.stdout, 0)
    print('-' * 50)
    rootb.export(sys.stdout, 0)

test()