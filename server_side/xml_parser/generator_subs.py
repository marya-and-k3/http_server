#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
  Author:   --<alexandr69@gmail.com>
  Purpose:
  Created: 03/16/21
"""
import os
import sys
from lxml import etree as etree_

import xml_parser.generator as supermod


def parsexml_(infile, parser=None, **kwargs):
    if parser is None:
        # Use the lxml ElementTree compatible parser so that, e.g.,
        #   we ignore comments.
        parser = etree_.ETCompatXMLParser()
    try:
        if isinstance(infile, os.PathLike):
            infile = os.path.join(infile)
    except AttributeError:
        pass
    doc = etree_.parse(infile, parser=parser, **kwargs)
    return doc


def parsexmlstring_(instring, parser=None, **kwargs):
    if parser is None:
        # Используйте парсер, совместимый с lxml ElementTree, чтобы, например,
        #   игнорировать комментарии.
        try:
            parser = etree_.ETCompatXMLParser()
        except AttributeError:
            # запасной вариант для xml.etree
            parser = etree_.XMLParser()
    element = etree_.fromstring(instring, parser=parser, **kwargs)
    return element

#
# Globals
#


ExternalEncoding = ''
SaveElementTreeNode = True

#
# Классы представления данных
#


class CalculationDtoSub(supermod.CalculationDto):
    def __init__(self, CADParams=None, IssueDiscountList=None, Items=None, PriceDate=None, MR3ShopId=None, CatalogId=None, PriceListId=None, PriceZoneId=None, Discounts=None, Actions=None, BillParams=None, **kwargs_):
        super(CalculationDtoSub, self).__init__(CADParams, IssueDiscountList, Items, PriceDate,
                                                MR3ShopId, CatalogId, PriceListId, PriceZoneId, Discounts, Actions, BillParams, **kwargs_)


supermod.CalculationDto.subclass = CalculationDtoSub
# end class CalculationDtoSub


class ItemsTypeSub(supermod.ItemsType):
    def __init__(self, CalculationItemDto=None, **kwargs_):
        super(ItemsTypeSub, self).__init__(CalculationItemDto, **kwargs_)


supermod.ItemsType.subclass = ItemsTypeSub
# end class ItemsTypeSub


class CalculationItemDtoTypeSub(supermod.CalculationItemDtoType):
    def __init__(self, Id=None, ParentId=None, SourceType=None, Nst=None, SortOrder=None, WareId=None, ModelId=None, WareName=None, WareParams=None, DwgParams=None, NstParams=None, CatalogParams=None, CADParams=None, Unit=None, Qty=None, QtyByP=None, Delta=None, CalculationItemType=None, PriceKind=None, Notes=None, Total=None, TotalWithSale=None, SalePtc=None, LastChangeDate=None, IssueDiscountList=None, BillParams=None, B2CSales=None, Discounts=None, NSTTypes=None, Works=None, **kwargs_):
        super(CalculationItemDtoTypeSub, self).__init__(Id, ParentId, SourceType, Nst, SortOrder, WareId, ModelId, WareName, WareParams, DwgParams, NstParams, CatalogParams, CADParams, Unit,
                                                        Qty, QtyByP, Delta, CalculationItemType, PriceKind, Notes, Total, TotalWithSale, SalePtc, LastChangeDate, IssueDiscountList, BillParams, B2CSales, Discounts, NSTTypes, Works, **kwargs_)


supermod.CalculationItemDtoType.subclass = CalculationItemDtoTypeSub
# end class CalculationItemDtoTypeSub


class CADParamsTypeSub(supermod.CADParamsType):
    def __init__(self, Lookup=None, **kwargs_):
        super(CADParamsTypeSub, self).__init__(Lookup, **kwargs_)


supermod.CADParamsType.subclass = CADParamsTypeSub
# end class CADParamsTypeSub


class LookupTypeSub(supermod.LookupType):
    def __init__(self, Key=None, KeyName=None, Value=None, **kwargs_):
        super(LookupTypeSub, self).__init__(Key, KeyName, Value, **kwargs_)


supermod.LookupType.subclass = LookupTypeSub
# end class LookupTypeSub


def get_root_tag(node):
    tag = supermod.Tag_pattern_.match(node.tag).groups()[-1]
    rootClass = None
    rootClass = supermod.GDSClassesMapping.get(tag)
    if rootClass is None and hasattr(supermod, tag):
        rootClass = getattr(supermod, tag)
    return tag, rootClass


def parse(inFilename, silence=False):
    parser = None
    doc = parsexml_(inFilename, parser)
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'CalculationDto'
        rootClass = supermod.CalculationDto
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    # Enable Python to collect the space used by the DOM.
    if not SaveElementTreeNode:
        doc = None
        rootNode = None
    if not silence:
        sys.stdout.write('<?xml version="1.0" ?>\n')
        rootObj.export(
            sys.stdout, 0, name_=rootTag,
            namespacedef_='',
            pretty_print=True)
    return rootObj


def parseEtree(inFilename, silence=False):
    parser = None
    doc = parsexml_(inFilename, parser)
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'CalculationDto'
        rootClass = supermod.CalculationDto
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    mapping = {}
    rootElement = rootObj.to_etree(None, name_=rootTag, mapping_=mapping)
    reverse_mapping = rootObj.gds_reverse_node_mapping(mapping)
    # Enable Python to collect the space used by the DOM.
    if not SaveElementTreeNode:
        doc = None
        rootNode = None
    if not silence:
        content = etree_.tostring(
            rootElement, pretty_print=True,
            xml_declaration=True, encoding="utf-8")
        sys.stdout.write(content)
        sys.stdout.write('\n')
    return rootObj, rootElement, mapping, reverse_mapping


def parseString(inString, silence=False):
    if sys.version_info.major == 2:
        from StringIO import StringIO
    else:
        from io import BytesIO as StringIO
    parser = None
    rootNode = parsexmlstring_(inString, parser)
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'CalculationDto'
        rootClass = supermod.CalculationDto
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    # Enable Python to collect the space used by the DOM.
    if not SaveElementTreeNode:
        rootNode = None
    if not silence:
        sys.stdout.write('<?xml version="1.0" ?>\n')
        rootObj.export(
            sys.stdout, 0, name_=rootTag,
            namespacedef_='')
    return rootObj


def parseLiteral(inFilename, silence=False):
    parser = None
    doc = parsexml_(inFilename, parser)
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'CalculationDto'
        rootClass = supermod.CalculationDto
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    # Enable Python to collect the space used by the DOM.
    if not SaveElementTreeNode:
        doc = None
        rootNode = None
    if not silence:
        sys.stdout.write('#from generator import *\n\n')
        sys.stdout.write('import generator as model_\n\n')
        sys.stdout.write('rootObj = model_.rootClass(\n')
        rootObj.exportLiteral(sys.stdout, 0, name_=rootTag)
        sys.stdout.write(')\n')
    return rootObj


USAGE_TEXT = """
Usage: python generator.py <infilename>
"""


def usage():
    print(USAGE_TEXT)
    sys.exit(1)


def main():
    args = sys.argv[1:]
    if len(args) != 1:
        usage()
    infilename = args[0]
    parse(infilename)


if __name__ == '__main__':
    #import pdb; pdb.set_trace()
    main()
