#!/usr/bin/env python3
#coding:utf-8
"""
  Author:  Aleksandr Dragunkin --<alexandr69@gmail.com>
  Purpose:
  Created: 03/16/21
"""

import sys

sys.path.extend(
    [r'c:\PKM_CG8\server_side\site-packages',
    r'c:\PKM_CG8\server_side\xml_parser',
    r'c:\PKM_CG8\server_side\xml_parser\new_variant']
)

import generator
import generator_subs

def test():
    #generator.supermod.CurrentSubclassModule_ = generator
    roota = generator.parse(r'c:\Users\a-dragunkin\Documents\MARIA_KITCHEN\sample-2021-05-26\N04.json.esp.soap.xml', silence=True)
    generator_subs.supermod.CurrentSubclassModule_ = generator_subs
    rootb = generator_subs.parse(r'c:\Users\a-dragunkin\Documents\MARIA_KITCHEN\sample-2021-05-26\N04.json.esp.soap.xml', silence=True)
    roota.export(sys.stdout, 0)
    print('-' * 50)
    rootb.export(sys.stdout, 0)

test()