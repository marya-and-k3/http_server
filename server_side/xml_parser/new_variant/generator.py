#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
  Author:   --<alexandr69@gmail.com>
  Purpose: 
  Created: 06/16/21
"""
import sys
try:
    ModulenotfoundExp_ = ModuleNotFoundError
except NameError:
    ModulenotfoundExp_ = ImportError
from six.moves import zip_longest
import os
import sys
import re as re_
import base64
import datetime as datetime_
import decimal as decimal_
try:
    from lxml import etree as etree_
except ImportError:
    from xml.etree import ElementTree as etree_


Validate_simpletypes_ = True
SaveElementTreeNode = True
if sys.version_info.major == 2:
    BaseStrType_ = basestring
else:
    BaseStrType_ = str


def parsexml_(infile, parser=None, **kwargs):
    if parser is None:
        # Use the lxml ElementTree compatible parser so that, e.g.,
        #   we ignore comments.
        try:
            parser = etree_.ETCompatXMLParser()
        except AttributeError:
            # fallback to xml.etree
            parser = etree_.XMLParser()
    try:
        if isinstance(infile, os.PathLike):
            infile = os.path.join(infile)
    except AttributeError:
        pass
    doc = etree_.parse(infile, parser=parser, **kwargs)
    return doc

def parsexmlstring_(instring, parser=None, **kwargs):
    if parser is None:
        # Use the lxml ElementTree compatible parser so that, e.g.,
        #   we ignore comments.
        try:
            parser = etree_.ETCompatXMLParser()
        except AttributeError:
            # fallback to xml.etree
            parser = etree_.XMLParser()
    element = etree_.fromstring(instring, parser=parser, **kwargs)
    return element

#
# Таблица определения префикса пространства имен (и других атрибутов)
#
# Модуль generatedsnamespaces, если он импортируется, должен содержать
# словарь с именем GeneratedsNamespaceDefs.  Этот словарь Python должен
# сопоставлять имена типов элементов (строки) с определениями префиксов
# пространства имен XML - схемы.  Метод экспорта для любого класса, для
# которого существует определение префикса пространства имен, будет
# экспортировать это определение в XML-представлении этого элемента.
# Пример использования этой таблицы см. в методе экспорта любого
# сгенерированного класса типа элемента.
# Примерная таблица:
#
#     # File: generatedsnamespaces.py
#
#     GenerateDSNamespaceDefs = {
#         "ElementtypeA": "http://www.xxx.com/namespaceA",
#         "ElementtypeB": "http://www.xxx.com/namespaceB",
#     }
#
# Кроме того, модуль generatedsnamespaces может содержать словарь python с
# именем GenerateDSNamespaceTypePrefixes, который связывает типы элементов с
# префиксами пространства имен, которые должны быть добавлены к значению
# атрибута "xsi:type".  См. Метод exportAttributes любого сгенерированного
# типа элемента и генерацию "xsi:type" для примера использования этой таблицы.
# Пример таблицы:
#
#     # File: generatedsnamespaces.py
#
#     GenerateDSNamespaceTypePrefixes = {
#         "ElementtypeC": "aaa:",
#         "ElementtypeD": "bbb:",
#     }
#

try:
    from generatedsnamespaces import GenerateDSNamespaceDefs as GenerateDSNamespaceDefs_
except ImportError:
    GenerateDSNamespaceDefs_ = {}
try:
    from generatedsnamespaces import GenerateDSNamespaceTypePrefixes as GenerateDSNamespaceTypePrefixes_
except ImportError:
    GenerateDSNamespaceTypePrefixes_ = {}

#
# Вы можете заменить следующее определение класса определением импортируемого
# модуля с именем "generatedscollector", содержащего класс с
# именем "GdsCollector".  См. Определение класса по умолчанию ниже для получения
# подсказок о возможном содержании этого класса.
#
try:
    from generatedscollector import GdsCollector as GdsCollector_
except ImportError:

    class GdsCollector_(object):

        def __init__(self, messages=None):
            if messages is None:
                self.messages = []
            else:
                self.messages = messages

        def add_message(self, msg):
            self.messages.append(msg)

        def get_messages(self):
            return self.messages

        def clear_messages(self):
            self.messages = []

        def print_messages(self):
            for msg in self.messages:
                print("Warning: {}".format(msg))

        def write_messages(self, outstream):
            for msg in self.messages:
                outstream.write("Warning: {}\n".format(msg))


#
# Суперкласс для перечисляемых типов
#

try:
    from enum import Enum
except ImportError:
    Enum = object

#
# Корневой суперкласс для классов типов элементов
#
# Вызовы методов в этих классах генерируются по умолчанию Можно заменить 
# эти методы, повторно реализовав следующий класс в модуле с именем 
# generatedssuper.py.

try:
    from generatedssuper import GeneratedsSuper
except ImportError as exp:
    
    class GeneratedsSuper(object):
        __hash__ = object.__hash__
        tzoff_pattern = re_.compile(r'(\+|-)((0\d|1[0-3]):[0-5]\d|14:00)$')
        class _FixedOffsetTZ(datetime_.tzinfo):
            def __init__(self, offset, name):
                self.__offset = datetime_.timedelta(minutes=offset)
                self.__name = name
            def utcoffset(self, dt):
                return self.__offset
            def tzname(self, dt):
                return self.__name
            def dst(self, dt):
                return None
        def gds_format_string(self, input_data, input_name=''):
            return input_data
        def gds_parse_string(self, input_data, node=None, input_name=''):
            return input_data
        def gds_validate_string(self, input_data, node=None, input_name=''):
            if not input_data:
                return ''
            else:
                return input_data
        def gds_format_base64(self, input_data, input_name=''):
            return base64.b64encode(input_data)
        def gds_validate_base64(self, input_data, node=None, input_name=''):
            return input_data
        def gds_format_integer(self, input_data, input_name=''):
            return '%d' % input_data
        def gds_parse_integer(self, input_data, node=None, input_name=''):
            try:
                ival = int(input_data)
            except (TypeError, ValueError) as exp:
                raise_parse_error(node, 'Требуется целочисленное значение: %s' % exp)
            return ival
        def gds_validate_integer(self, input_data, node=None, input_name=''):
            try:
                value = int(input_data)
            except (TypeError, ValueError):
                raise_parse_error(node, 'Requires integer value')
            return value
        def gds_format_integer_list(self, input_data, input_name=''):
            return '%s' % ' '.join(input_data)
        def gds_validate_integer_list(
                self, input_data, node=None, input_name=''):
            values = input_data.split()
            for value in values:
                try:
                    int(value)
                except (TypeError, ValueError):
                    raise_parse_error(node, 'Требуется последовательность целочисленных значений')
            return values
        def gds_format_float(self, input_data, input_name=''):
            return ('%.15f' % input_data).rstrip('0')
        def gds_parse_float(self, input_data, node=None, input_name=''):
            try:
                fval_ = float(input_data)
            except (TypeError, ValueError) as exp:
                raise_parse_error(node, 'Требуется вещественное значение или double: %s' % exp)
            return fval_
        def gds_validate_float(self, input_data, node=None, input_name=''):
            try:
                value = float(input_data)
            except (TypeError, ValueError):
                raise_parse_error(node, 'Требуется вещественное значение')
            return value
        def gds_format_float_list(self, input_data, input_name=''):
            return '%s' % ' '.join(input_data)
        def gds_validate_float_list(
                self, input_data, node=None, input_name=''):
            values = input_data.split()
            for value in values:
                try:
                    float(value)
                except (TypeError, ValueError):
                    raise_parse_error(node, 'Требуется последовательность вещественных значений')
            return values
        def gds_format_decimal(self, input_data, input_name=''):
            return ('%0.10f' % input_data).rstrip('0')
        def gds_parse_decimal(self, input_data, node=None, input_name=''):
            try:
                decimal_value = decimal_.Decimal(input_data)
            except (TypeError, ValueError):
                raise_parse_error(node, 'Требуется десятичное значение')
            return decimal_value
        def gds_validate_decimal(self, input_data, node=None, input_name=''):
            try:
                value = decimal_.Decimal(input_data)
            except (TypeError, ValueError):
                raise_parse_error(node, 'Требуется десятичное значение')
            return value
        def gds_format_decimal_list(self, input_data, input_name=''):
            return '%s' % ' '.join(input_data)
        def gds_validate_decimal_list(
                self, input_data, node=None, input_name=''):
            values = input_data.split()
            for value in values:
                try:
                    decimal_.Decimal(value)
                except (TypeError, ValueError):
                    raise_parse_error(node, 'Требуется последовательность десятичных значений')
            return values
        def gds_format_double(self, input_data, input_name=''):
            return '%e' % input_data
        def gds_parse_double(self, input_data, node=None, input_name=''):
            try:
                fval_ = float(input_data)
            except (TypeError, ValueError) as exp:
                raise_parse_error(node, 'Требуется значение типа double или float: %s' % exp)
            return fval_
        def gds_validate_double(self, input_data, node=None, input_name=''):
            try:
                value = float(input_data)
            except (TypeError, ValueError):
                raise_parse_error(node, 'Требуется значение типа double или float')
            return value
        def gds_format_double_list(self, input_data, input_name=''):
            return '%s' % ' '.join(input_data)
        def gds_validate_double_list(
                self, input_data, node=None, input_name=''):
            values = input_data.split()
            for value in values:
                try:
                    float(value)
                except (TypeError, ValueError):
                    raise_parse_error(
                        node, 'Требуется последовательность значений типа double или float')
            return values
        def gds_format_boolean(self, input_data, input_name=''):
            return ('%s' % input_data).lower()
        def gds_parse_boolean(self, input_data, node=None, input_name=''):
            if input_data in ('true', '1'):
                bval = True
            elif input_data in ('false', '0'):
                bval = False
            else:
                raise_parse_error(node, 'Требуется логическое значение')
            return bval
        def gds_validate_boolean(self, input_data, node=None, input_name=''):
            if input_data not in (True, 1, False, 0, ):
                raise_parse_error(
                    node,
                    'Требуется логическое значение '
                    '(один из True, 1, False, 0)')
            return input_data
        def gds_format_boolean_list(self, input_data, input_name=''):
            return '%s' % ' '.join(input_data)
        def gds_validate_boolean_list(
                self, input_data, node=None, input_name=''):
            values = input_data.split()
            for value in values:
                if value not in (True, 1, False, 0, ):
                    raise_parse_error(
                        node,
                        'Требуется последовательность логических значений '
                        '(один из True, 1, False, 0)')
            return values
        def gds_validate_datetime(self, input_data, node=None, input_name=''):
            return input_data
        def gds_format_datetime(self, input_data, input_name=''):
            if input_data.microsecond == 0:
                _svalue = '%04d-%02d-%02dT%02d:%02d:%02d' % (
                    input_data.year,
                    input_data.month,
                    input_data.day,
                    input_data.hour,
                    input_data.minute,
                    input_data.second,
                )
            else:
                _svalue = '%04d-%02d-%02dT%02d:%02d:%02d.%s' % (
                    input_data.year,
                    input_data.month,
                    input_data.day,
                    input_data.hour,
                    input_data.minute,
                    input_data.second,
                    ('%f' % (float(input_data.microsecond) / 1000000))[2:],
                )
            if input_data.tzinfo is not None:
                tzoff = input_data.tzinfo.utcoffset(input_data)
                if tzoff is not None:
                    total_seconds = tzoff.seconds + (86400 * tzoff.days)
                    if total_seconds == 0:
                        _svalue += 'Z'
                    else:
                        if total_seconds < 0:
                            _svalue += '-'
                            total_seconds *= -1
                        else:
                            _svalue += '+'
                        hours = total_seconds // 3600
                        minutes = (total_seconds - (hours * 3600)) // 60
                        _svalue += '{0:02d}:{1:02d}'.format(hours, minutes)
            return _svalue
        @classmethod
        def gds_parse_datetime(cls, input_data):
            tz = None
            if input_data[-1] == 'Z':
                tz = GeneratedsSuper._FixedOffsetTZ(0, 'UTC')
                input_data = input_data[:-1]
            else:
                results = GeneratedsSuper.tzoff_pattern.search(input_data)
                if results is not None:
                    tzoff_parts = results.group(2).split(':')
                    tzoff = int(tzoff_parts[0]) * 60 + int(tzoff_parts[1])
                    if results.group(1) == '-':
                        tzoff *= -1
                    tz = GeneratedsSuper._FixedOffsetTZ(
                        tzoff, results.group(0))
                    input_data = input_data[:-6]
            time_parts = input_data.split('.')
            if len(time_parts) > 1:
                micro_seconds = int(float('0.' + time_parts[1]) * 1000000)
                input_data = '%s.%s' % (
                    time_parts[0], "{}".format(micro_seconds).rjust(6, "0"), )
                dt = datetime_.datetime.strptime(
                    input_data, '%Y-%m-%dT%H:%M:%S.%f')
            else:
                dt = datetime_.datetime.strptime(
                    input_data, '%Y-%m-%dT%H:%M:%S')
            dt = dt.replace(tzinfo=tz)
            return dt
        def gds_validate_date(self, input_data, node=None, input_name=''):
            return input_data
        def gds_format_date(self, input_data, input_name=''):
            _svalue = '%04d-%02d-%02d' % (
                input_data.year,
                input_data.month,
                input_data.day,
            )
            try:
                if input_data.tzinfo is not None:
                    tzoff = input_data.tzinfo.utcoffset(input_data)
                    if tzoff is not None:
                        total_seconds = tzoff.seconds + (86400 * tzoff.days)
                        if total_seconds == 0:
                            _svalue += 'Z'
                        else:
                            if total_seconds < 0:
                                _svalue += '-'
                                total_seconds *= -1
                            else:
                                _svalue += '+'
                            hours = total_seconds // 3600
                            minutes = (total_seconds - (hours * 3600)) // 60
                            _svalue += '{0:02d}:{1:02d}'.format(
                                hours, minutes)
            except AttributeError:
                pass
            return _svalue
        @classmethod
        def gds_parse_date(cls, input_data):
            tz = None
            if input_data[-1] == 'Z':
                tz = GeneratedsSuper._FixedOffsetTZ(0, 'UTC')
                input_data = input_data[:-1]
            else:
                results = GeneratedsSuper.tzoff_pattern.search(input_data)
                if results is not None:
                    tzoff_parts = results.group(2).split(':')
                    tzoff = int(tzoff_parts[0]) * 60 + int(tzoff_parts[1])
                    if results.group(1) == '-':
                        tzoff *= -1
                    tz = GeneratedsSuper._FixedOffsetTZ(
                        tzoff, results.group(0))
                    input_data = input_data[:-6]
            dt = datetime_.datetime.strptime(input_data, '%Y-%m-%d')
            dt = dt.replace(tzinfo=tz)
            return dt.date()
        def gds_validate_time(self, input_data, node=None, input_name=''):
            return input_data
        def gds_format_time(self, input_data, input_name=''):
            if input_data.microsecond == 0:
                _svalue = '%02d:%02d:%02d' % (
                    input_data.hour,
                    input_data.minute,
                    input_data.second,
                )
            else:
                _svalue = '%02d:%02d:%02d.%s' % (
                    input_data.hour,
                    input_data.minute,
                    input_data.second,
                    ('%f' % (float(input_data.microsecond) / 1000000))[2:],
                )
            if input_data.tzinfo is not None:
                tzoff = input_data.tzinfo.utcoffset(input_data)
                if tzoff is not None:
                    total_seconds = tzoff.seconds + (86400 * tzoff.days)
                    if total_seconds == 0:
                        _svalue += 'Z'
                    else:
                        if total_seconds < 0:
                            _svalue += '-'
                            total_seconds *= -1
                        else:
                            _svalue += '+'
                        hours = total_seconds // 3600
                        minutes = (total_seconds - (hours * 3600)) // 60
                        _svalue += '{0:02d}:{1:02d}'.format(hours, minutes)
            return _svalue
        def gds_validate_simple_patterns(self, patterns, target):
            # pat is a list of lists of strings/patterns.
            # The target value must match at least one of the patterns
            # in order for the test to succeed.
            found1 = True
            for patterns1 in patterns:
                found2 = False
                for patterns2 in patterns1:
                    mo = re_.search(patterns2, target)
                    if mo is not None and len(mo.group(0)) == len(target):
                        found2 = True
                        break
                if not found2:
                    found1 = False
                    break
            return found1
        @classmethod
        def gds_parse_time(cls, input_data):
            tz = None
            if input_data[-1] == 'Z':
                tz = GeneratedsSuper._FixedOffsetTZ(0, 'UTC')
                input_data = input_data[:-1]
            else:
                results = GeneratedsSuper.tzoff_pattern.search(input_data)
                if results is not None:
                    tzoff_parts = results.group(2).split(':')
                    tzoff = int(tzoff_parts[0]) * 60 + int(tzoff_parts[1])
                    if results.group(1) == '-':
                        tzoff *= -1
                    tz = GeneratedsSuper._FixedOffsetTZ(
                        tzoff, results.group(0))
                    input_data = input_data[:-6]
            if len(input_data.split('.')) > 1:
                dt = datetime_.datetime.strptime(input_data, '%H:%M:%S.%f')
            else:
                dt = datetime_.datetime.strptime(input_data, '%H:%M:%S')
            dt = dt.replace(tzinfo=tz)
            return dt.time()
        def gds_check_cardinality_(
                self, value, input_name,
                min_occurs=0, max_occurs=1, required=None):
            if value is None:
                length = 0
            elif isinstance(value, list):
                length = len(value)
            else:
                length = 1
            if required is not None :
                if required and length < 1:
                    self.gds_collector_.add_message(
                        "Обязательное значение {}{} отсутствует".format(
                            input_name, self.gds_get_node_lineno_()))
            if length < min_occurs:
                self.gds_collector_.add_message(
                    "Number of values for {}{} is below "
                    "the minimum allowed, "
                    "expected at least {}, found {}".format(
                        input_name, self.gds_get_node_lineno_(),
                        min_occurs, length))
            elif length > max_occurs:
                self.gds_collector_.add_message(
                    "Number of values for {}{} is above "
                    "the maximum allowed, "
                    "expected at most {}, found {}".format(
                        input_name, self.gds_get_node_lineno_(),
                        max_occurs, length))
        def gds_validate_builtin_ST_(
                self, validator, value, input_name,
                min_occurs=None, max_occurs=None, required=None):
            if value is not None:
                try:
                    validator(value, input_name=input_name)
                except GDSParseError as parse_error:
                    self.gds_collector_.add_message(str(parse_error))
        def gds_validate_defined_ST_(
                self, validator, value, input_name,
                min_occurs=None, max_occurs=None, required=None):
            if value is not None:
                try:
                    validator(value)
                except GDSParseError as parse_error:
                    self.gds_collector_.add_message(str(parse_error))
        def gds_str_lower(self, instring):
            return instring.lower()
        def get_path_(self, node):
            path_list = []
            self.get_path_list_(node, path_list)
            path_list.reverse()
            path = '/'.join(path_list)
            return path
        Tag_strip_pattern_ = re_.compile(r'\{.*\}')
        def get_path_list_(self, node, path_list):
            if node is None:
                return
            tag = GeneratedsSuper.Tag_strip_pattern_.sub('', node.tag)
            if tag:
                path_list.append(tag)
            self.get_path_list_(node.getparent(), path_list)
        def get_class_obj_(self, node, default_class=None):
            class_obj1 = default_class
            if 'xsi' in node.nsmap:
                classname = node.get('{%s}type' % node.nsmap['xsi'])
                if classname is not None:
                    names = classname.split(':')
                    if len(names) == 2:
                        classname = names[1]
                    class_obj2 = globals().get(classname)
                    if class_obj2 is not None:
                        class_obj1 = class_obj2
            return class_obj1
        def gds_build_any(self, node, type_name=None):
            # provide default value in case option --disable-xml is used.
            content = ""
            content = etree_.tostring(node, encoding="unicode")
            return content
        @classmethod
        def gds_reverse_node_mapping(cls, mapping):
            return dict(((v, k) for k, v in mapping.items()))
        @staticmethod
        def gds_encode(instring):
            if sys.version_info.major == 2:
                if ExternalEncoding:
                    encoding = ExternalEncoding
                else:
                    encoding = 'utf-8'
                return instring.encode(encoding)
            else:
                return instring
        @staticmethod
        def convert_unicode(instring):
            if isinstance(instring, str):
                result = quote_xml(instring)
            elif sys.version_info.major == 2 and isinstance(instring, unicode):
                result = quote_xml(instring).encode('utf8')
            else:
                result = GeneratedsSuper.gds_encode(str(instring))
            return result
        def __eq__(self, other):
            def excl_select_objs_(obj):
                return (obj[0] != 'parent_object_' and
                        obj[0] != 'gds_collector_')
            if type(self) != type(other):
                return False
            return all(x == y for x, y in zip_longest(
                filter(excl_select_objs_, self.__dict__.items()),
                filter(excl_select_objs_, other.__dict__.items())))
        def __ne__(self, other):
            return not self.__eq__(other)
        # Django ETL transform hooks.
        def gds_djo_etl_transform(self):
            pass
        def gds_djo_etl_transform_db_obj(self, dbobj):
            pass
        # SQLAlchemy ETL transform hooks.
        def gds_sqa_etl_transform(self):
            return 0, None
        def gds_sqa_etl_transform_db_obj(self, dbobj):
            pass
        def gds_get_node_lineno_(self):
            if (hasattr(self, "gds_elementtree_node_") and
                    self.gds_elementtree_node_ is not None):
                return ' near line {}'.format(
                    self.gds_elementtree_node_.sourceline)
            else:
                return ""
    
    
    def getSubclassFromModule_(module, class_):
        '''Get the subclass of a class from a specific module.'''
        name = class_.__name__ + 'Sub'
        if hasattr(module, name):
            return getattr(module, name)
        else:
            return None


#
# Если у вас  установлен IPython, вы можете раскомментировать и использовать 
# следующее.  IPython доступен по адресу http://ipython.scipy.org/.
#

## from IPython.Shell import IPShellEmbed
## args = ''
## ipshell = IPShellEmbed(args,
##     banner = 'Dropping into IPython',
##     exit_msg = 'Leaving Interpreter, back to program.')

# Затем используйте следующую строку, где и когда надо перейти в
# IPython shell:
#    ipshell('<some message> -- Entering ipshell.\nHit Ctrl-D to exit')

#
# Globals
#

ExternalEncoding = ''
# Установите это значение в false, чтобы деактивировать во 
# время экспорта использование префиксов пространства имен, 
# захваченных из входного документа.
UseCapturedNS_ = True
CapturedNsmap_ = {}
Tag_pattern_ = re_.compile(r'({.*})?(.*)')
String_cleanup_pat_ = re_.compile(r"[\n\r\s]+")
Namespace_extract_pat_ = re_.compile(r'{(.*)}(.*)')
CDATA_pattern_ = re_.compile(r"<!\[CDATA\[.*?\]\]>", re_.DOTALL)

# Измените None, чтобы перенаправить сгенерированный модуль суперкласса на 
# использование конкретного модуля подкласса.
CurrentSubclassModule_ = None

#
# Support/utility functions.
#


def showIndent(outfile, level, pretty_print=True):
    if pretty_print:
        for idx in range(level):
            outfile.write('    ')


def quote_xml(inStr):
    "Экранируйте символы разметки, но не изменяйте разделы CDATA."
    if not inStr:
        return ''
    s1 = (isinstance(inStr, BaseStrType_) and inStr or '%s' % inStr)
    s2 = ''
    pos = 0
    matchobjects = CDATA_pattern_.finditer(s1)
    for mo in matchobjects:
        s3 = s1[pos:mo.start()]
        s2 += quote_xml_aux(s3)
        s2 += s1[mo.start():mo.end()]
        pos = mo.end()
    s3 = s1[pos:]
    s2 += quote_xml_aux(s3)
    return s2


def quote_xml_aux(inStr):
    s1 = inStr.replace('&', '&amp;')
    s1 = s1.replace('<', '&lt;')
    s1 = s1.replace('>', '&gt;')
    return s1


def quote_attrib(inStr):
    s1 = (isinstance(inStr, BaseStrType_) and inStr or '%s' % inStr)
    s1 = s1.replace('&', '&amp;')
    s1 = s1.replace('<', '&lt;')
    s1 = s1.replace('>', '&gt;')
    if '"' in s1:
        if "'" in s1:
            s1 = '"%s"' % s1.replace('"', "&quot;")
        else:
            s1 = "'%s'" % s1
    else:
        s1 = '"%s"' % s1
    return s1


def quote_python(inStr):
    s1 = inStr
    if s1.find("'") == -1:
        if s1.find('\n') == -1:
            return "'%s'" % s1
        else:
            return "'''%s'''" % s1
    else:
        if s1.find('"') != -1:
            s1 = s1.replace('"', '\\"')
        if s1.find('\n') == -1:
            return '"%s"' % s1
        else:
            return '"""%s"""' % s1


def get_all_text_(node):
    if node.text is not None:
        text = node.text
    else:
        text = ''
    for child in node:
        if child.tail is not None:
            text += child.tail
    return text


def find_attr_value_(attr_name, node):
    attrs = node.attrib
    attr_parts = attr_name.split(':')
    value = None
    if len(attr_parts) == 1:
        value = attrs.get(attr_name)
    elif len(attr_parts) == 2:
        prefix, name = attr_parts
        namespace = node.nsmap.get(prefix)
        if namespace is not None:
            value = attrs.get('{%s}%s' % (namespace, name, ))
    return value


def encode_str_2_3(instr):
    return instr


class GDSParseError(Exception):
    pass


def raise_parse_error(node, msg):
    if node is not None:
        msg = '%s (element %s/line %d)' % (msg, node.tag, node.sourceline, )
    raise GDSParseError(msg)


class MixedContainer:
    # Constants for category:
    CategoryNone = 0
    CategoryText = 1
    CategorySimple = 2
    CategoryComplex = 3
    # Constants for content_type:
    TypeNone = 0
    TypeText = 1
    TypeString = 2
    TypeInteger = 3
    TypeFloat = 4
    TypeDecimal = 5
    TypeDouble = 6
    TypeBoolean = 7
    TypeBase64 = 8
    def __init__(self, category, content_type, name, value):
        self.category = category
        self.content_type = content_type
        self.name = name
        self.value = value
    def getCategory(self):
        return self.category
    def getContenttype(self, content_type):
        return self.content_type
    def getValue(self):
        return self.value
    def getName(self):
        return self.name
    def export(self, outfile, level, name, namespace,
               pretty_print=True):
        if self.category == MixedContainer.CategoryText:
            # Prevent exporting empty content as empty lines.
            if self.value.strip():
                outfile.write(self.value)
        elif self.category == MixedContainer.CategorySimple:
            self.exportSimple(outfile, level, name)
        else:    # category == MixedContainer.CategoryComplex
            self.value.export(
                outfile, level, namespace, name_=name,
                pretty_print=pretty_print)
    def exportSimple(self, outfile, level, name):
        if self.content_type == MixedContainer.TypeString:
            outfile.write('<%s>%s</%s>' % (
                self.name, self.value, self.name))
        elif self.content_type == MixedContainer.TypeInteger or \
                self.content_type == MixedContainer.TypeBoolean:
            outfile.write('<%s>%d</%s>' % (
                self.name, self.value, self.name))
        elif self.content_type == MixedContainer.TypeFloat or \
                self.content_type == MixedContainer.TypeDecimal:
            outfile.write('<%s>%f</%s>' % (
                self.name, self.value, self.name))
        elif self.content_type == MixedContainer.TypeDouble:
            outfile.write('<%s>%g</%s>' % (
                self.name, self.value, self.name))
        elif self.content_type == MixedContainer.TypeBase64:
            outfile.write('<%s>%s</%s>' % (
                self.name,
                base64.b64encode(self.value),
                self.name))
    def to_etree(self, element):
        if self.category == MixedContainer.CategoryText:
            # Prevent exporting empty content as empty lines.
            if self.value.strip():
                if len(element) > 0:
                    if element[-1].tail is None:
                        element[-1].tail = self.value
                    else:
                        element[-1].tail += self.value
                else:
                    if element.text is None:
                        element.text = self.value
                    else:
                        element.text += self.value
        elif self.category == MixedContainer.CategorySimple:
            subelement = etree_.SubElement(
                element, '%s' % self.name)
            subelement.text = self.to_etree_simple()
        else:    # category == MixedContainer.CategoryComplex
            self.value.to_etree(element)
    def to_etree_simple(self):
        if self.content_type == MixedContainer.TypeString:
            text = self.value
        elif (self.content_type == MixedContainer.TypeInteger or
                self.content_type == MixedContainer.TypeBoolean):
            text = '%d' % self.value
        elif (self.content_type == MixedContainer.TypeFloat or
                self.content_type == MixedContainer.TypeDecimal):
            text = '%f' % self.value
        elif self.content_type == MixedContainer.TypeDouble:
            text = '%g' % self.value
        elif self.content_type == MixedContainer.TypeBase64:
            text = '%s' % base64.b64encode(self.value)
        return text
    def exportLiteral(self, outfile, level, name):
        if self.category == MixedContainer.CategoryText:
            showIndent(outfile, level)
            outfile.write(
                'model_.MixedContainer(%d, %d, "%s", "%s"),\n' % (
                    self.category, self.content_type,
                    self.name, self.value))
        elif self.category == MixedContainer.CategorySimple:
            showIndent(outfile, level)
            outfile.write(
                'model_.MixedContainer(%d, %d, "%s", "%s"),\n' % (
                    self.category, self.content_type,
                    self.name, self.value))
        else:    # category == MixedContainer.CategoryComplex
            showIndent(outfile, level)
            outfile.write(
                'model_.MixedContainer(%d, %d, "%s",\n' % (
                    self.category, self.content_type, self.name,))
            self.value.exportLiteral(outfile, level + 1)
            showIndent(outfile, level)
            outfile.write(')\n')


class MemberSpec_(object):
    def __init__(self, name='', data_type='', container=0,
            optional=0, child_attrs=None, choice=None):
        self.name = name
        self.data_type = data_type
        self.container = container
        self.child_attrs = child_attrs
        self.choice = choice
        self.optional = optional
    def set_name(self, name): self.name = name
    def get_name(self): return self.name
    def set_data_type(self, data_type): self.data_type = data_type
    def get_data_type_chain(self): return self.data_type
    def get_data_type(self):
        if isinstance(self.data_type, list):
            if len(self.data_type) > 0:
                return self.data_type[-1]
            else:
                return 'xs:string'
        else:
            return self.data_type
    def set_container(self, container): self.container = container
    def get_container(self): return self.container
    def set_child_attrs(self, child_attrs): self.child_attrs = child_attrs
    def get_child_attrs(self): return self.child_attrs
    def set_choice(self, choice): self.choice = choice
    def get_choice(self): return self.choice
    def set_optional(self, optional): self.optional = optional
    def get_optional(self): return self.optional


def _cast(typ, value):
    if typ is None or value is None:
        return value
    return typ(value)

#
# Data representation classes.
#


class CalculationDto(GeneratedsSuper):
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, xmlns_xsd=None, xmlns_xsi=None, CADParams=None, IssueDiscountList=None, Items=None, PriceDate=None, MR3ShopId=None, CatalogId=None, PriceListId=None, PriceZoneId=None, Discounts=None, Actions=None, BillParams=None, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = None
        self.xmlns_xsd = _cast(None, xmlns_xsd)
        self.xmlns_xsd_nsprefix_ = None
        self.xmlns_xsi = _cast(None, xmlns_xsi)
        self.xmlns_xsi_nsprefix_ = None
        self.CADParams = CADParams
        self.CADParams_nsprefix_ = None
        self.IssueDiscountList = IssueDiscountList
        self.IssueDiscountList_nsprefix_ = None
        self.Items = Items
        self.Items_nsprefix_ = None
        if isinstance(PriceDate, BaseStrType_):
            initvalue_ = datetime_.datetime.strptime(PriceDate, '%Y-%m-%dT%H:%M:%S')
        else:
            initvalue_ = PriceDate
        self.PriceDate = initvalue_
        self.PriceDate_nsprefix_ = None
        self.MR3ShopId = MR3ShopId
        self.MR3ShopId_nsprefix_ = None
        self.CatalogId = CatalogId
        self.CatalogId_nsprefix_ = None
        self.PriceListId = PriceListId
        self.PriceListId_nsprefix_ = None
        self.PriceZoneId = PriceZoneId
        self.PriceZoneId_nsprefix_ = None
        self.Discounts = Discounts
        self.Discounts_nsprefix_ = None
        self.Actions = Actions
        self.Actions_nsprefix_ = None
        self.BillParams = BillParams
        self.BillParams_nsprefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, CalculationDto)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if CalculationDto.subclass:
            return CalculationDto.subclass(*args_, **kwargs_)
        else:
            return CalculationDto(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def get_CADParams(self):
        return self.CADParams
    def set_CADParams(self, CADParams):
        self.CADParams = CADParams
    def get_IssueDiscountList(self):
        return self.IssueDiscountList
    def set_IssueDiscountList(self, IssueDiscountList):
        self.IssueDiscountList = IssueDiscountList
    def get_Items(self):
        return self.Items
    def set_Items(self, Items):
        self.Items = Items
    def get_PriceDate(self):
        return self.PriceDate
    def set_PriceDate(self, PriceDate):
        self.PriceDate = PriceDate
    def get_MR3ShopId(self):
        return self.MR3ShopId
    def set_MR3ShopId(self, MR3ShopId):
        self.MR3ShopId = MR3ShopId
    def get_CatalogId(self):
        return self.CatalogId
    def set_CatalogId(self, CatalogId):
        self.CatalogId = CatalogId
    def get_PriceListId(self):
        return self.PriceListId
    def set_PriceListId(self, PriceListId):
        self.PriceListId = PriceListId
    def get_PriceZoneId(self):
        return self.PriceZoneId
    def set_PriceZoneId(self, PriceZoneId):
        self.PriceZoneId = PriceZoneId
    def get_Discounts(self):
        return self.Discounts
    def set_Discounts(self, Discounts):
        self.Discounts = Discounts
    def get_Actions(self):
        return self.Actions
    def set_Actions(self, Actions):
        self.Actions = Actions
    def get_BillParams(self):
        return self.BillParams
    def set_BillParams(self, BillParams):
        self.BillParams = BillParams
    def get_xmlns_xsd(self):
        return self.xmlns_xsd
    def set_xmlns_xsd(self, xmlns_xsd):
        self.xmlns_xsd = xmlns_xsd
    def get_xmlns_xsi(self):
        return self.xmlns_xsi
    def set_xmlns_xsi(self, xmlns_xsi):
        self.xmlns_xsi = xmlns_xsi
    def hasContent_(self):
        if (
            self.CADParams is not None or
            self.IssueDiscountList is not None or
            self.Items is not None or
            self.PriceDate is not None or
            self.MR3ShopId is not None or
            self.CatalogId is not None or
            self.PriceListId is not None or
            self.PriceZoneId is not None or
            self.Discounts is not None or
            self.Actions is not None or
            self.BillParams is not None
        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='CalculationDto', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('CalculationDto')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None:
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='CalculationDto')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='CalculationDto', pretty_print=pretty_print)
            showIndent(outfile, level, pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='CalculationDto'):
        if self.xmlns_xsd is not None and 'xmlns_xsd' not in already_processed:
            already_processed.add('xmlns_xsd')
            outfile.write(' xmlns:xsd=%s' % (self.gds_encode(self.gds_format_string(quote_attrib(self.xmlns_xsd), input_name='xmlns:xsd')), ))
        if self.xmlns_xsi is not None and 'xmlns_xsi' not in already_processed:
            already_processed.add('xmlns_xsi')
            outfile.write(' xmlns:xsi=%s' % (self.gds_encode(self.gds_format_string(quote_attrib(self.xmlns_xsi), input_name='xmlns:xsi')), ))
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='CalculationDto', fromsubclass_=False, pretty_print=True):
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.CADParams is not None:
            namespaceprefix_ = self.CADParams_nsprefix_ + ':' if (UseCapturedNS_ and self.CADParams_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sCADParams>%s</%sCADParams>%s' % (namespaceprefix_ , self.gds_encode(self.gds_format_string(quote_xml(self.CADParams), input_name='CADParams')), namespaceprefix_ , eol_))
        if self.IssueDiscountList is not None:
            namespaceprefix_ = self.IssueDiscountList_nsprefix_ + ':' if (UseCapturedNS_ and self.IssueDiscountList_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sIssueDiscountList>%s</%sIssueDiscountList>%s' % (namespaceprefix_ , self.gds_encode(self.gds_format_string(quote_xml(self.IssueDiscountList), input_name='IssueDiscountList')), namespaceprefix_ , eol_))
        if self.Items is not None:
            namespaceprefix_ = self.Items_nsprefix_ + ':' if (UseCapturedNS_ and self.Items_nsprefix_) else ''
            self.Items.export(outfile, level, namespaceprefix_, namespacedef_='', name_='Items', pretty_print=pretty_print)
        if self.PriceDate is not None:
            namespaceprefix_ = self.PriceDate_nsprefix_ + ':' if (UseCapturedNS_ and self.PriceDate_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sPriceDate>%s</%sPriceDate>%s' % (namespaceprefix_ , self.gds_format_datetime(self.PriceDate, input_name='PriceDate'), namespaceprefix_ , eol_))
        if self.MR3ShopId is not None:
            namespaceprefix_ = self.MR3ShopId_nsprefix_ + ':' if (UseCapturedNS_ and self.MR3ShopId_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sMR3ShopId>%s</%sMR3ShopId>%s' % (namespaceprefix_ , self.gds_format_integer(self.MR3ShopId, input_name='MR3ShopId'), namespaceprefix_ , eol_))
        if self.CatalogId is not None:
            namespaceprefix_ = self.CatalogId_nsprefix_ + ':' if (UseCapturedNS_ and self.CatalogId_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sCatalogId>%s</%sCatalogId>%s' % (namespaceprefix_ , self.gds_format_integer(self.CatalogId, input_name='CatalogId'), namespaceprefix_ , eol_))
        if self.PriceListId is not None:
            namespaceprefix_ = self.PriceListId_nsprefix_ + ':' if (UseCapturedNS_ and self.PriceListId_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sPriceListId>%s</%sPriceListId>%s' % (namespaceprefix_ , self.gds_format_integer(self.PriceListId, input_name='PriceListId'), namespaceprefix_ , eol_))
        if self.PriceZoneId is not None:
            namespaceprefix_ = self.PriceZoneId_nsprefix_ + ':' if (UseCapturedNS_ and self.PriceZoneId_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sPriceZoneId>%s</%sPriceZoneId>%s' % (namespaceprefix_ , self.gds_format_integer(self.PriceZoneId, input_name='PriceZoneId'), namespaceprefix_ , eol_))
        if self.Discounts is not None:
            namespaceprefix_ = self.Discounts_nsprefix_ + ':' if (UseCapturedNS_ and self.Discounts_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sDiscounts>%s</%sDiscounts>%s' % (namespaceprefix_ , self.gds_encode(self.gds_format_string(quote_xml(self.Discounts), input_name='Discounts')), namespaceprefix_ , eol_))
        if self.Actions is not None:
            namespaceprefix_ = self.Actions_nsprefix_ + ':' if (UseCapturedNS_ and self.Actions_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sActions>%s</%sActions>%s' % (namespaceprefix_ , self.gds_encode(self.gds_format_string(quote_xml(self.Actions), input_name='Actions')), namespaceprefix_ , eol_))
        if self.BillParams is not None:
            namespaceprefix_ = self.BillParams_nsprefix_ + ':' if (UseCapturedNS_ and self.BillParams_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sBillParams>%s</%sBillParams>%s' % (namespaceprefix_ , self.gds_encode(self.gds_format_string(quote_xml(self.BillParams), input_name='BillParams')), namespaceprefix_ , eol_))
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        value = find_attr_value_('xmlns:xsd', node)
        if value is not None and 'xmlns:xsd' not in already_processed:
            already_processed.add('xmlns:xsd')
            self.xmlns_xsd = value
        value = find_attr_value_('xmlns:xsi', node)
        if value is not None and 'xmlns:xsi' not in already_processed:
            already_processed.add('xmlns:xsi')
            self.xmlns_xsi = value
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        if nodeName_ == 'CADParams':
            value_ = child_.text
            value_ = self.gds_parse_string(value_, node, 'CADParams')
            value_ = self.gds_validate_string(value_, node, 'CADParams')
            self.CADParams = value_
            self.CADParams_nsprefix_ = child_.prefix
        elif nodeName_ == 'IssueDiscountList':
            value_ = child_.text
            value_ = self.gds_parse_string(value_, node, 'IssueDiscountList')
            value_ = self.gds_validate_string(value_, node, 'IssueDiscountList')
            self.IssueDiscountList = value_
            self.IssueDiscountList_nsprefix_ = child_.prefix
        elif nodeName_ == 'Items':
            obj_ = ItemsType.factory(parent_object_=self)
            obj_.build(child_, gds_collector_=gds_collector_)
            self.Items = obj_
            obj_.original_tagname_ = 'Items'
        elif nodeName_ == 'PriceDate':
            sval_ = child_.text
            dval_ = self.gds_parse_datetime(sval_)
            self.PriceDate = dval_
            self.PriceDate_nsprefix_ = child_.prefix
        elif nodeName_ == 'MR3ShopId' and child_.text:
            sval_ = child_.text
            ival_ = self.gds_parse_integer(sval_, node, 'MR3ShopId')
            ival_ = self.gds_validate_integer(ival_, node, 'MR3ShopId')
            self.MR3ShopId = ival_
            self.MR3ShopId_nsprefix_ = child_.prefix
        elif nodeName_ == 'CatalogId' and child_.text:
            sval_ = child_.text
            ival_ = self.gds_parse_integer(sval_, node, 'CatalogId')
            ival_ = self.gds_validate_integer(ival_, node, 'CatalogId')
            self.CatalogId = ival_
            self.CatalogId_nsprefix_ = child_.prefix
        elif nodeName_ == 'PriceListId' and child_.text:
            sval_ = child_.text
            ival_ = self.gds_parse_integer(sval_, node, 'PriceListId')
            ival_ = self.gds_validate_integer(ival_, node, 'PriceListId')
            self.PriceListId = ival_
            self.PriceListId_nsprefix_ = child_.prefix
        elif nodeName_ == 'PriceZoneId' and child_.text:
            sval_ = child_.text
            ival_ = self.gds_parse_integer(sval_, node, 'PriceZoneId')
            ival_ = self.gds_validate_integer(ival_, node, 'PriceZoneId')
            self.PriceZoneId = ival_
            self.PriceZoneId_nsprefix_ = child_.prefix
        elif nodeName_ == 'Discounts':
            value_ = child_.text
            value_ = self.gds_parse_string(value_, node, 'Discounts')
            value_ = self.gds_validate_string(value_, node, 'Discounts')
            self.Discounts = value_
            self.Discounts_nsprefix_ = child_.prefix
        elif nodeName_ == 'Actions':
            value_ = child_.text
            value_ = self.gds_parse_string(value_, node, 'Actions')
            value_ = self.gds_validate_string(value_, node, 'Actions')
            self.Actions = value_
            self.Actions_nsprefix_ = child_.prefix
        elif nodeName_ == 'BillParams':
            value_ = child_.text
            value_ = self.gds_parse_string(value_, node, 'BillParams')
            value_ = self.gds_validate_string(value_, node, 'BillParams')
            self.BillParams = value_
            self.BillParams_nsprefix_ = child_.prefix
# end class CalculationDto


class CADParams(GeneratedsSuper):
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, CADParams)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if CADParams.subclass:
            return CADParams.subclass(*args_, **kwargs_)
        else:
            return CADParams(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def hasContent_(self):
        if (

        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='CADParams', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('CADParams')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None:
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='CADParams')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='CADParams', pretty_print=pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='CADParams'):
        pass
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='CADParams', fromsubclass_=False, pretty_print=True):
        pass
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        pass
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        pass
# end class CADParams


class IssueDiscountList(GeneratedsSuper):
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, IssueDiscountList)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if IssueDiscountList.subclass:
            return IssueDiscountList.subclass(*args_, **kwargs_)
        else:
            return IssueDiscountList(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def hasContent_(self):
        if (

        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='IssueDiscountList', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('IssueDiscountList')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None:
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='IssueDiscountList')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='IssueDiscountList', pretty_print=pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='IssueDiscountList'):
        pass
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='IssueDiscountList', fromsubclass_=False, pretty_print=True):
        pass
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        pass
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        pass
# end class IssueDiscountList


class Discounts(GeneratedsSuper):
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, Discounts)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if Discounts.subclass:
            return Discounts.subclass(*args_, **kwargs_)
        else:
            return Discounts(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def hasContent_(self):
        if (

        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='Discounts', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('Discounts')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None:
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='Discounts')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='Discounts', pretty_print=pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='Discounts'):
        pass
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='Discounts', fromsubclass_=False, pretty_print=True):
        pass
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        pass
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        pass
# end class Discounts


class Actions(GeneratedsSuper):
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, Actions)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if Actions.subclass:
            return Actions.subclass(*args_, **kwargs_)
        else:
            return Actions(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def hasContent_(self):
        if (

        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='Actions', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('Actions')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None:
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='Actions')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='Actions', pretty_print=pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='Actions'):
        pass
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='Actions', fromsubclass_=False, pretty_print=True):
        pass
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        pass
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        pass
# end class Actions


class BillParams(GeneratedsSuper):
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, BillParams)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if BillParams.subclass:
            return BillParams.subclass(*args_, **kwargs_)
        else:
            return BillParams(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def hasContent_(self):
        if (

        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='BillParams', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('BillParams')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None:
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='BillParams')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='BillParams', pretty_print=pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='BillParams'):
        pass
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='BillParams', fromsubclass_=False, pretty_print=True):
        pass
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        pass
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        pass
# end class BillParams


class ItemsType(GeneratedsSuper):
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, CalculationItemDto=None, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = None
        if CalculationItemDto is None:
            self.CalculationItemDto = []
        else:
            self.CalculationItemDto = CalculationItemDto
        self.CalculationItemDto_nsprefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, ItemsType)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if ItemsType.subclass:
            return ItemsType.subclass(*args_, **kwargs_)
        else:
            return ItemsType(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def get_CalculationItemDto(self):
        return self.CalculationItemDto
    def set_CalculationItemDto(self, CalculationItemDto):
        self.CalculationItemDto = CalculationItemDto
    def add_CalculationItemDto(self, value):
        self.CalculationItemDto.append(value)
    def insert_CalculationItemDto_at(self, index, value):
        self.CalculationItemDto.insert(index, value)
    def replace_CalculationItemDto_at(self, index, value):
        self.CalculationItemDto[index] = value
    def hasContent_(self):
        if (
            self.CalculationItemDto
        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='ItemsType', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('ItemsType')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None:
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='ItemsType')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='ItemsType', pretty_print=pretty_print)
            showIndent(outfile, level, pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='ItemsType'):
        pass
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='ItemsType', fromsubclass_=False, pretty_print=True):
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        for CalculationItemDto_ in self.CalculationItemDto:
            namespaceprefix_ = self.CalculationItemDto_nsprefix_ + ':' if (UseCapturedNS_ and self.CalculationItemDto_nsprefix_) else ''
            CalculationItemDto_.export(outfile, level, namespaceprefix_, namespacedef_='', name_='CalculationItemDto', pretty_print=pretty_print)
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        pass
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        if nodeName_ == 'CalculationItemDto':
            obj_ = CalculationItemDtoType.factory(parent_object_=self)
            obj_.build(child_, gds_collector_=gds_collector_)
            self.CalculationItemDto.append(obj_)
            obj_.original_tagname_ = 'CalculationItemDto'
# end class ItemsType


class CalculationItemDtoType(GeneratedsSuper):
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, Id=None, SourceType=None, Source=None, Nst=None, SortOrder=None, WareId=None, ModelId=None, WareName=None, WareParams=None, DwgParams=None, NstParams=None, CatalogParams=None, CADParams=None, Unit=None, Qty=None, QtyByP=None, Delta=None, CalculationItemType=None, PriceKind=None, Notes=None, Total=None, TotalWithSale=None, SalePtc=None, LastChangeDate=None, IssueDiscountList=None, BillParams=None, B2CSales=None, Discounts=None, NSTTypes=None, Works=None, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = None
        self.Id = Id
        self.Id_nsprefix_ = None
        self.SourceType = SourceType
        self.SourceType_nsprefix_ = None
        self.Source = Source
        self.Source_nsprefix_ = None
        self.Nst = Nst
        self.Nst_nsprefix_ = None
        self.SortOrder = SortOrder
        self.SortOrder_nsprefix_ = None
        self.WareId = WareId
        self.WareId_nsprefix_ = None
        self.ModelId = ModelId
        self.ModelId_nsprefix_ = None
        self.WareName = WareName
        self.WareName_nsprefix_ = None
        self.WareParams = WareParams
        self.WareParams_nsprefix_ = None
        self.DwgParams = DwgParams
        self.DwgParams_nsprefix_ = None
        self.NstParams = NstParams
        self.NstParams_nsprefix_ = None
        self.CatalogParams = CatalogParams
        self.CatalogParams_nsprefix_ = None
        self.CADParams = CADParams
        self.CADParams_nsprefix_ = None
        self.Unit = Unit
        self.Unit_nsprefix_ = None
        self.Qty = Qty
        self.Qty_nsprefix_ = None
        self.QtyByP = QtyByP
        self.QtyByP_nsprefix_ = None
        self.Delta = Delta
        self.Delta_nsprefix_ = None
        self.CalculationItemType = CalculationItemType
        self.CalculationItemType_nsprefix_ = None
        self.PriceKind = PriceKind
        self.PriceKind_nsprefix_ = None
        self.Notes = Notes
        self.Notes_nsprefix_ = None
        self.Total = Total
        self.Total_nsprefix_ = None
        self.TotalWithSale = TotalWithSale
        self.TotalWithSale_nsprefix_ = None
        self.SalePtc = SalePtc
        self.SalePtc_nsprefix_ = None
        self.LastChangeDate = LastChangeDate
        self.LastChangeDate_nsprefix_ = None
        self.IssueDiscountList = IssueDiscountList
        self.IssueDiscountList_nsprefix_ = None
        self.BillParams = BillParams
        self.BillParams_nsprefix_ = None
        self.B2CSales = B2CSales
        self.B2CSales_nsprefix_ = None
        self.Discounts = Discounts
        self.Discounts_nsprefix_ = None
        self.NSTTypes = NSTTypes
        self.NSTTypes_nsprefix_ = None
        self.Works = Works
        self.Works_nsprefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, CalculationItemDtoType)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if CalculationItemDtoType.subclass:
            return CalculationItemDtoType.subclass(*args_, **kwargs_)
        else:
            return CalculationItemDtoType(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def get_Id(self):
        return self.Id
    def set_Id(self, Id):
        self.Id = Id
    def get_SourceType(self):
        return self.SourceType
    def set_SourceType(self, SourceType):
        self.SourceType = SourceType
    def get_Source(self):
        return self.Source
    def set_Source(self, Source):
        self.Source = Source
    def get_Nst(self):
        return self.Nst
    def set_Nst(self, Nst):
        self.Nst = Nst
    def get_SortOrder(self):
        return self.SortOrder
    def set_SortOrder(self, SortOrder):
        self.SortOrder = SortOrder
    def get_WareId(self):
        return self.WareId
    def set_WareId(self, WareId):
        self.WareId = WareId
    def get_ModelId(self):
        return self.ModelId
    def set_ModelId(self, ModelId):
        self.ModelId = ModelId
    def get_WareName(self):
        return self.WareName
    def set_WareName(self, WareName):
        self.WareName = WareName
    def get_WareParams(self):
        return self.WareParams
    def set_WareParams(self, WareParams):
        self.WareParams = WareParams
    def get_DwgParams(self):
        return self.DwgParams
    def set_DwgParams(self, DwgParams):
        self.DwgParams = DwgParams
    def get_NstParams(self):
        return self.NstParams
    def set_NstParams(self, NstParams):
        self.NstParams = NstParams
    def get_CatalogParams(self):
        return self.CatalogParams
    def set_CatalogParams(self, CatalogParams):
        self.CatalogParams = CatalogParams
    def get_CADParams(self):
        return self.CADParams
    def set_CADParams(self, CADParams):
        self.CADParams = CADParams
    def get_Unit(self):
        return self.Unit
    def set_Unit(self, Unit):
        self.Unit = Unit
    def get_Qty(self):
        return self.Qty
    def set_Qty(self, Qty):
        self.Qty = Qty
    def get_QtyByP(self):
        return self.QtyByP
    def set_QtyByP(self, QtyByP):
        self.QtyByP = QtyByP
    def get_Delta(self):
        return self.Delta
    def set_Delta(self, Delta):
        self.Delta = Delta
    def get_CalculationItemType(self):
        return self.CalculationItemType
    def set_CalculationItemType(self, CalculationItemType):
        self.CalculationItemType = CalculationItemType
    def get_PriceKind(self):
        return self.PriceKind
    def set_PriceKind(self, PriceKind):
        self.PriceKind = PriceKind
    def get_Notes(self):
        return self.Notes
    def set_Notes(self, Notes):
        self.Notes = Notes
    def get_Total(self):
        return self.Total
    def set_Total(self, Total):
        self.Total = Total
    def get_TotalWithSale(self):
        return self.TotalWithSale
    def set_TotalWithSale(self, TotalWithSale):
        self.TotalWithSale = TotalWithSale
    def get_SalePtc(self):
        return self.SalePtc
    def set_SalePtc(self, SalePtc):
        self.SalePtc = SalePtc
    def get_LastChangeDate(self):
        return self.LastChangeDate
    def set_LastChangeDate(self, LastChangeDate):
        self.LastChangeDate = LastChangeDate
    def get_IssueDiscountList(self):
        return self.IssueDiscountList
    def set_IssueDiscountList(self, IssueDiscountList):
        self.IssueDiscountList = IssueDiscountList
    def get_BillParams(self):
        return self.BillParams
    def set_BillParams(self, BillParams):
        self.BillParams = BillParams
    def get_B2CSales(self):
        return self.B2CSales
    def set_B2CSales(self, B2CSales):
        self.B2CSales = B2CSales
    def get_Discounts(self):
        return self.Discounts
    def set_Discounts(self, Discounts):
        self.Discounts = Discounts
    def get_NSTTypes(self):
        return self.NSTTypes
    def set_NSTTypes(self, NSTTypes):
        self.NSTTypes = NSTTypes
    def get_Works(self):
        return self.Works
    def set_Works(self, Works):
        self.Works = Works
    def hasContent_(self):
        if (
            self.Id is not None or
            self.SourceType is not None or
            self.Source is not None or
            self.Nst is not None or
            self.SortOrder is not None or
            self.WareId is not None or
            self.ModelId is not None or
            self.WareName is not None or
            self.WareParams is not None or
            self.DwgParams is not None or
            self.NstParams is not None or
            self.CatalogParams is not None or
            self.CADParams is not None or
            self.Unit is not None or
            self.Qty is not None or
            self.QtyByP is not None or
            self.Delta is not None or
            self.CalculationItemType is not None or
            self.PriceKind is not None or
            self.Notes is not None or
            self.Total is not None or
            self.TotalWithSale is not None or
            self.SalePtc is not None or
            self.LastChangeDate is not None or
            self.IssueDiscountList is not None or
            self.BillParams is not None or
            self.B2CSales is not None or
            self.Discounts is not None or
            self.NSTTypes is not None or
            self.Works is not None
        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='CalculationItemDtoType', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('CalculationItemDtoType')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None:
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='CalculationItemDtoType')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='CalculationItemDtoType', pretty_print=pretty_print)
            showIndent(outfile, level, pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='CalculationItemDtoType'):
        pass
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='CalculationItemDtoType', fromsubclass_=False, pretty_print=True):
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.Id is not None:
            namespaceprefix_ = self.Id_nsprefix_ + ':' if (UseCapturedNS_ and self.Id_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sId>%s</%sId>%s' % (namespaceprefix_ , self.gds_encode(self.gds_format_string(quote_xml(self.Id), input_name='Id')), namespaceprefix_ , eol_))
        if self.SourceType is not None:
            namespaceprefix_ = self.SourceType_nsprefix_ + ':' if (UseCapturedNS_ and self.SourceType_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sSourceType>%s</%sSourceType>%s' % (namespaceprefix_ , self.gds_encode(self.gds_format_string(quote_xml(self.SourceType), input_name='SourceType')), namespaceprefix_ , eol_))
        if self.Source is not None:
            namespaceprefix_ = self.Source_nsprefix_ + ':' if (UseCapturedNS_ and self.Source_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sSource>%s</%sSource>%s' % (namespaceprefix_ , self.gds_encode(self.gds_format_string(quote_xml(self.Source), input_name='Source')), namespaceprefix_ , eol_))
        if self.Nst is not None:
            namespaceprefix_ = self.Nst_nsprefix_ + ':' if (UseCapturedNS_ and self.Nst_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sNst>%s</%sNst>%s' % (namespaceprefix_ , self.gds_encode(self.gds_format_string(quote_xml(self.Nst), input_name='Nst')), namespaceprefix_ , eol_))
        if self.SortOrder is not None:
            namespaceprefix_ = self.SortOrder_nsprefix_ + ':' if (UseCapturedNS_ and self.SortOrder_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sSortOrder>%s</%sSortOrder>%s' % (namespaceprefix_ , self.gds_format_integer(self.SortOrder, input_name='SortOrder'), namespaceprefix_ , eol_))
        if self.WareId is not None:
            namespaceprefix_ = self.WareId_nsprefix_ + ':' if (UseCapturedNS_ and self.WareId_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sWareId>%s</%sWareId>%s' % (namespaceprefix_ , self.gds_format_integer(self.WareId, input_name='WareId'), namespaceprefix_ , eol_))
        if self.ModelId is not None:
            namespaceprefix_ = self.ModelId_nsprefix_ + ':' if (UseCapturedNS_ and self.ModelId_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sModelId>%s</%sModelId>%s' % (namespaceprefix_ , self.gds_format_integer(self.ModelId, input_name='ModelId'), namespaceprefix_ , eol_))
        if self.WareName is not None:
            namespaceprefix_ = self.WareName_nsprefix_ + ':' if (UseCapturedNS_ and self.WareName_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sWareName>%s</%sWareName>%s' % (namespaceprefix_ , self.gds_encode(self.gds_format_string(quote_xml(self.WareName), input_name='WareName')), namespaceprefix_ , eol_))
        if self.WareParams is not None:
            namespaceprefix_ = self.WareParams_nsprefix_ + ':' if (UseCapturedNS_ and self.WareParams_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sWareParams>%s</%sWareParams>%s' % (namespaceprefix_ , self.gds_encode(self.gds_format_string(quote_xml(self.WareParams), input_name='WareParams')), namespaceprefix_ , eol_))
        if self.DwgParams is not None:
            namespaceprefix_ = self.DwgParams_nsprefix_ + ':' if (UseCapturedNS_ and self.DwgParams_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sDwgParams>%s</%sDwgParams>%s' % (namespaceprefix_ , self.gds_encode(self.gds_format_string(quote_xml(self.DwgParams), input_name='DwgParams')), namespaceprefix_ , eol_))
        if self.NstParams is not None:
            namespaceprefix_ = self.NstParams_nsprefix_ + ':' if (UseCapturedNS_ and self.NstParams_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sNstParams>%s</%sNstParams>%s' % (namespaceprefix_ , self.gds_encode(self.gds_format_string(quote_xml(self.NstParams), input_name='NstParams')), namespaceprefix_ , eol_))
        if self.CatalogParams is not None:
            namespaceprefix_ = self.CatalogParams_nsprefix_ + ':' if (UseCapturedNS_ and self.CatalogParams_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sCatalogParams>%s</%sCatalogParams>%s' % (namespaceprefix_ , self.gds_encode(self.gds_format_string(quote_xml(self.CatalogParams), input_name='CatalogParams')), namespaceprefix_ , eol_))
        if self.CADParams is not None:
            namespaceprefix_ = self.CADParams_nsprefix_ + ':' if (UseCapturedNS_ and self.CADParams_nsprefix_) else ''
            self.CADParams.export(outfile, level, namespaceprefix_, namespacedef_='', name_='CADParams', pretty_print=pretty_print)
        if self.Unit is not None:
            namespaceprefix_ = self.Unit_nsprefix_ + ':' if (UseCapturedNS_ and self.Unit_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sUnit>%s</%sUnit>%s' % (namespaceprefix_ , self.gds_encode(self.gds_format_string(quote_xml(self.Unit), input_name='Unit')), namespaceprefix_ , eol_))
        if self.Qty is not None:
            namespaceprefix_ = self.Qty_nsprefix_ + ':' if (UseCapturedNS_ and self.Qty_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sQty>%s</%sQty>%s' % (namespaceprefix_ , self.gds_format_integer(self.Qty, input_name='Qty'), namespaceprefix_ , eol_))
        if self.QtyByP is not None:
            namespaceprefix_ = self.QtyByP_nsprefix_ + ':' if (UseCapturedNS_ and self.QtyByP_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sQtyByP>%s</%sQtyByP>%s' % (namespaceprefix_ , self.gds_format_integer(self.QtyByP, input_name='QtyByP'), namespaceprefix_ , eol_))
        if self.Delta is not None:
            namespaceprefix_ = self.Delta_nsprefix_ + ':' if (UseCapturedNS_ and self.Delta_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sDelta>%s</%sDelta>%s' % (namespaceprefix_ , self.gds_format_integer(self.Delta, input_name='Delta'), namespaceprefix_ , eol_))
        if self.CalculationItemType is not None:
            namespaceprefix_ = self.CalculationItemType_nsprefix_ + ':' if (UseCapturedNS_ and self.CalculationItemType_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sCalculationItemType>%s</%sCalculationItemType>%s' % (namespaceprefix_ , self.gds_format_integer(self.CalculationItemType, input_name='CalculationItemType'), namespaceprefix_ , eol_))
        if self.PriceKind is not None:
            namespaceprefix_ = self.PriceKind_nsprefix_ + ':' if (UseCapturedNS_ and self.PriceKind_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sPriceKind>%s</%sPriceKind>%s' % (namespaceprefix_ , self.gds_encode(self.gds_format_string(quote_xml(self.PriceKind), input_name='PriceKind')), namespaceprefix_ , eol_))
        if self.Notes is not None:
            namespaceprefix_ = self.Notes_nsprefix_ + ':' if (UseCapturedNS_ and self.Notes_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sNotes>%s</%sNotes>%s' % (namespaceprefix_ , self.gds_encode(self.gds_format_string(quote_xml(self.Notes), input_name='Notes')), namespaceprefix_ , eol_))
        if self.Total is not None:
            namespaceprefix_ = self.Total_nsprefix_ + ':' if (UseCapturedNS_ and self.Total_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sTotal>%s</%sTotal>%s' % (namespaceprefix_ , self.gds_format_integer(self.Total, input_name='Total'), namespaceprefix_ , eol_))
        if self.TotalWithSale is not None:
            namespaceprefix_ = self.TotalWithSale_nsprefix_ + ':' if (UseCapturedNS_ and self.TotalWithSale_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sTotalWithSale>%s</%sTotalWithSale>%s' % (namespaceprefix_ , self.gds_format_integer(self.TotalWithSale, input_name='TotalWithSale'), namespaceprefix_ , eol_))
        if self.SalePtc is not None:
            namespaceprefix_ = self.SalePtc_nsprefix_ + ':' if (UseCapturedNS_ and self.SalePtc_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sSalePtc>%s</%sSalePtc>%s' % (namespaceprefix_ , self.gds_format_integer(self.SalePtc, input_name='SalePtc'), namespaceprefix_ , eol_))
        if self.LastChangeDate is not None:
            namespaceprefix_ = self.LastChangeDate_nsprefix_ + ':' if (UseCapturedNS_ and self.LastChangeDate_nsprefix_) else ''
            self.LastChangeDate.export(outfile, level, namespaceprefix_, namespacedef_='', name_='LastChangeDate', pretty_print=pretty_print)
        if self.IssueDiscountList is not None:
            namespaceprefix_ = self.IssueDiscountList_nsprefix_ + ':' if (UseCapturedNS_ and self.IssueDiscountList_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sIssueDiscountList>%s</%sIssueDiscountList>%s' % (namespaceprefix_ , self.gds_encode(self.gds_format_string(quote_xml(self.IssueDiscountList), input_name='IssueDiscountList')), namespaceprefix_ , eol_))
        if self.BillParams is not None:
            namespaceprefix_ = self.BillParams_nsprefix_ + ':' if (UseCapturedNS_ and self.BillParams_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sBillParams>%s</%sBillParams>%s' % (namespaceprefix_ , self.gds_encode(self.gds_format_string(quote_xml(self.BillParams), input_name='BillParams')), namespaceprefix_ , eol_))
        if self.B2CSales is not None:
            namespaceprefix_ = self.B2CSales_nsprefix_ + ':' if (UseCapturedNS_ and self.B2CSales_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sB2CSales>%s</%sB2CSales>%s' % (namespaceprefix_ , self.gds_encode(self.gds_format_string(quote_xml(self.B2CSales), input_name='B2CSales')), namespaceprefix_ , eol_))
        if self.Discounts is not None:
            namespaceprefix_ = self.Discounts_nsprefix_ + ':' if (UseCapturedNS_ and self.Discounts_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sDiscounts>%s</%sDiscounts>%s' % (namespaceprefix_ , self.gds_encode(self.gds_format_string(quote_xml(self.Discounts), input_name='Discounts')), namespaceprefix_ , eol_))
        if self.NSTTypes is not None:
            namespaceprefix_ = self.NSTTypes_nsprefix_ + ':' if (UseCapturedNS_ and self.NSTTypes_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sNSTTypes>%s</%sNSTTypes>%s' % (namespaceprefix_ , self.gds_encode(self.gds_format_string(quote_xml(self.NSTTypes), input_name='NSTTypes')), namespaceprefix_ , eol_))
        if self.Works is not None:
            namespaceprefix_ = self.Works_nsprefix_ + ':' if (UseCapturedNS_ and self.Works_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sWorks>%s</%sWorks>%s' % (namespaceprefix_ , self.gds_encode(self.gds_format_string(quote_xml(self.Works), input_name='Works')), namespaceprefix_ , eol_))
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        pass
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        if nodeName_ == 'Id':
            value_ = child_.text
            value_ = self.gds_parse_string(value_, node, 'Id')
            value_ = self.gds_validate_string(value_, node, 'Id')
            self.Id = value_
            self.Id_nsprefix_ = child_.prefix
        elif nodeName_ == 'SourceType':
            value_ = child_.text
            value_ = self.gds_parse_string(value_, node, 'SourceType')
            value_ = self.gds_validate_string(value_, node, 'SourceType')
            self.SourceType = value_
            self.SourceType_nsprefix_ = child_.prefix
        elif nodeName_ == 'Source':
            value_ = child_.text
            value_ = self.gds_parse_string(value_, node, 'Source')
            value_ = self.gds_validate_string(value_, node, 'Source')
            self.Source = value_
            self.Source_nsprefix_ = child_.prefix
        elif nodeName_ == 'Nst':
            value_ = child_.text
            value_ = self.gds_parse_string(value_, node, 'Nst')
            value_ = self.gds_validate_string(value_, node, 'Nst')
            self.Nst = value_
            self.Nst_nsprefix_ = child_.prefix
        elif nodeName_ == 'SortOrder' and child_.text:
            sval_ = child_.text
            ival_ = self.gds_parse_integer(sval_, node, 'SortOrder')
            ival_ = self.gds_validate_integer(ival_, node, 'SortOrder')
            self.SortOrder = ival_
            self.SortOrder_nsprefix_ = child_.prefix
        elif nodeName_ == 'WareId' and child_.text:
            sval_ = child_.text
            ival_ = self.gds_parse_integer(sval_, node, 'WareId')
            ival_ = self.gds_validate_integer(ival_, node, 'WareId')
            self.WareId = ival_
            self.WareId_nsprefix_ = child_.prefix
        elif nodeName_ == 'ModelId' and child_.text:
            sval_ = child_.text
            ival_ = self.gds_parse_integer(sval_, node, 'ModelId')
            ival_ = self.gds_validate_integer(ival_, node, 'ModelId')
            self.ModelId = ival_
            self.ModelId_nsprefix_ = child_.prefix
        elif nodeName_ == 'WareName':
            value_ = child_.text
            value_ = self.gds_parse_string(value_, node, 'WareName')
            value_ = self.gds_validate_string(value_, node, 'WareName')
            self.WareName = value_
            self.WareName_nsprefix_ = child_.prefix
        elif nodeName_ == 'WareParams':
            value_ = child_.text
            value_ = self.gds_parse_string(value_, node, 'WareParams')
            value_ = self.gds_validate_string(value_, node, 'WareParams')
            self.WareParams = value_
            self.WareParams_nsprefix_ = child_.prefix
        elif nodeName_ == 'DwgParams':
            value_ = child_.text
            value_ = self.gds_parse_string(value_, node, 'DwgParams')
            value_ = self.gds_validate_string(value_, node, 'DwgParams')
            self.DwgParams = value_
            self.DwgParams_nsprefix_ = child_.prefix
        elif nodeName_ == 'NstParams':
            value_ = child_.text
            value_ = self.gds_parse_string(value_, node, 'NstParams')
            value_ = self.gds_validate_string(value_, node, 'NstParams')
            self.NstParams = value_
            self.NstParams_nsprefix_ = child_.prefix
        elif nodeName_ == 'CatalogParams':
            value_ = child_.text
            value_ = self.gds_parse_string(value_, node, 'CatalogParams')
            value_ = self.gds_validate_string(value_, node, 'CatalogParams')
            self.CatalogParams = value_
            self.CatalogParams_nsprefix_ = child_.prefix
        elif nodeName_ == 'CADParams':
            obj_ = CADParamsType.factory(parent_object_=self)
            obj_.build(child_, gds_collector_=gds_collector_)
            self.CADParams = obj_
            obj_.original_tagname_ = 'CADParams'
        elif nodeName_ == 'Unit':
            value_ = child_.text
            value_ = self.gds_parse_string(value_, node, 'Unit')
            value_ = self.gds_validate_string(value_, node, 'Unit')
            self.Unit = value_
            self.Unit_nsprefix_ = child_.prefix
        elif nodeName_ == 'Qty' and child_.text:
            sval_ = child_.text
            ival_ = self.gds_parse_integer(sval_, node, 'Qty')
            ival_ = self.gds_validate_integer(ival_, node, 'Qty')
            self.Qty = ival_
            self.Qty_nsprefix_ = child_.prefix
        elif nodeName_ == 'QtyByP' and child_.text:
            sval_ = child_.text
            ival_ = self.gds_parse_integer(sval_, node, 'QtyByP')
            ival_ = self.gds_validate_integer(ival_, node, 'QtyByP')
            self.QtyByP = ival_
            self.QtyByP_nsprefix_ = child_.prefix
        elif nodeName_ == 'Delta' and child_.text:
            sval_ = child_.text
            ival_ = self.gds_parse_integer(sval_, node, 'Delta')
            ival_ = self.gds_validate_integer(ival_, node, 'Delta')
            self.Delta = ival_
            self.Delta_nsprefix_ = child_.prefix
        elif nodeName_ == 'CalculationItemType' and child_.text:
            sval_ = child_.text
            ival_ = self.gds_parse_integer(sval_, node, 'CalculationItemType')
            ival_ = self.gds_validate_integer(ival_, node, 'CalculationItemType')
            self.CalculationItemType = ival_
            self.CalculationItemType_nsprefix_ = child_.prefix
        elif nodeName_ == 'PriceKind':
            value_ = child_.text
            value_ = self.gds_parse_string(value_, node, 'PriceKind')
            value_ = self.gds_validate_string(value_, node, 'PriceKind')
            self.PriceKind = value_
            self.PriceKind_nsprefix_ = child_.prefix
        elif nodeName_ == 'Notes':
            value_ = child_.text
            value_ = self.gds_parse_string(value_, node, 'Notes')
            value_ = self.gds_validate_string(value_, node, 'Notes')
            self.Notes = value_
            self.Notes_nsprefix_ = child_.prefix
        elif nodeName_ == 'Total' and child_.text:
            sval_ = child_.text
            ival_ = self.gds_parse_integer(sval_, node, 'Total')
            ival_ = self.gds_validate_integer(ival_, node, 'Total')
            self.Total = ival_
            self.Total_nsprefix_ = child_.prefix
        elif nodeName_ == 'TotalWithSale' and child_.text:
            sval_ = child_.text
            ival_ = self.gds_parse_integer(sval_, node, 'TotalWithSale')
            ival_ = self.gds_validate_integer(ival_, node, 'TotalWithSale')
            self.TotalWithSale = ival_
            self.TotalWithSale_nsprefix_ = child_.prefix
        elif nodeName_ == 'SalePtc' and child_.text:
            sval_ = child_.text
            ival_ = self.gds_parse_integer(sval_, node, 'SalePtc')
            ival_ = self.gds_validate_integer(ival_, node, 'SalePtc')
            self.SalePtc = ival_
            self.SalePtc_nsprefix_ = child_.prefix
        elif nodeName_ == 'LastChangeDate':
            obj_ = LastChangeDateType.factory(parent_object_=self)
            obj_.build(child_, gds_collector_=gds_collector_)
            self.LastChangeDate = obj_
            obj_.original_tagname_ = 'LastChangeDate'
        elif nodeName_ == 'IssueDiscountList':
            value_ = child_.text
            value_ = self.gds_parse_string(value_, node, 'IssueDiscountList')
            value_ = self.gds_validate_string(value_, node, 'IssueDiscountList')
            self.IssueDiscountList = value_
            self.IssueDiscountList_nsprefix_ = child_.prefix
        elif nodeName_ == 'BillParams':
            value_ = child_.text
            value_ = self.gds_parse_string(value_, node, 'BillParams')
            value_ = self.gds_validate_string(value_, node, 'BillParams')
            self.BillParams = value_
            self.BillParams_nsprefix_ = child_.prefix
        elif nodeName_ == 'B2CSales':
            value_ = child_.text
            value_ = self.gds_parse_string(value_, node, 'B2CSales')
            value_ = self.gds_validate_string(value_, node, 'B2CSales')
            self.B2CSales = value_
            self.B2CSales_nsprefix_ = child_.prefix
        elif nodeName_ == 'Discounts':
            value_ = child_.text
            value_ = self.gds_parse_string(value_, node, 'Discounts')
            value_ = self.gds_validate_string(value_, node, 'Discounts')
            self.Discounts = value_
            self.Discounts_nsprefix_ = child_.prefix
        elif nodeName_ == 'NSTTypes':
            value_ = child_.text
            value_ = self.gds_parse_string(value_, node, 'NSTTypes')
            value_ = self.gds_validate_string(value_, node, 'NSTTypes')
            self.NSTTypes = value_
            self.NSTTypes_nsprefix_ = child_.prefix
        elif nodeName_ == 'Works':
            value_ = child_.text
            value_ = self.gds_parse_string(value_, node, 'Works')
            value_ = self.gds_validate_string(value_, node, 'Works')
            self.Works = value_
            self.Works_nsprefix_ = child_.prefix
# end class CalculationItemDtoType


class WareParams(GeneratedsSuper):
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, WareParams)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if WareParams.subclass:
            return WareParams.subclass(*args_, **kwargs_)
        else:
            return WareParams(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def hasContent_(self):
        if (

        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='WareParams', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('WareParams')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None:
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='WareParams')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='WareParams', pretty_print=pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='WareParams'):
        pass
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='WareParams', fromsubclass_=False, pretty_print=True):
        pass
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        pass
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        pass
# end class WareParams


class DwgParams(GeneratedsSuper):
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, DwgParams)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if DwgParams.subclass:
            return DwgParams.subclass(*args_, **kwargs_)
        else:
            return DwgParams(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def hasContent_(self):
        if (

        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='DwgParams', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('DwgParams')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None:
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='DwgParams')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='DwgParams', pretty_print=pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='DwgParams'):
        pass
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='DwgParams', fromsubclass_=False, pretty_print=True):
        pass
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        pass
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        pass
# end class DwgParams


class NstParams(GeneratedsSuper):
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, NstParams)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if NstParams.subclass:
            return NstParams.subclass(*args_, **kwargs_)
        else:
            return NstParams(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def hasContent_(self):
        if (

        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='NstParams', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('NstParams')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None:
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='NstParams')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='NstParams', pretty_print=pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='NstParams'):
        pass
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='NstParams', fromsubclass_=False, pretty_print=True):
        pass
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        pass
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        pass
# end class NstParams


class CatalogParams(GeneratedsSuper):
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, CatalogParams)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if CatalogParams.subclass:
            return CatalogParams.subclass(*args_, **kwargs_)
        else:
            return CatalogParams(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def hasContent_(self):
        if (

        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='CatalogParams', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('CatalogParams')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None:
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='CatalogParams')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='CatalogParams', pretty_print=pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='CatalogParams'):
        pass
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='CatalogParams', fromsubclass_=False, pretty_print=True):
        pass
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        pass
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        pass
# end class CatalogParams


class Notes(GeneratedsSuper):
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, Notes)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if Notes.subclass:
            return Notes.subclass(*args_, **kwargs_)
        else:
            return Notes(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def hasContent_(self):
        if (

        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='Notes', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('Notes')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None:
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='Notes')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='Notes', pretty_print=pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='Notes'):
        pass
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='Notes', fromsubclass_=False, pretty_print=True):
        pass
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        pass
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        pass
# end class Notes


class B2CSales(GeneratedsSuper):
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, B2CSales)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if B2CSales.subclass:
            return B2CSales.subclass(*args_, **kwargs_)
        else:
            return B2CSales(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def hasContent_(self):
        if (

        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='B2CSales', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('B2CSales')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None:
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='B2CSales')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='B2CSales', pretty_print=pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='B2CSales'):
        pass
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='B2CSales', fromsubclass_=False, pretty_print=True):
        pass
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        pass
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        pass
# end class B2CSales


class NSTTypes(GeneratedsSuper):
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, NSTTypes)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if NSTTypes.subclass:
            return NSTTypes.subclass(*args_, **kwargs_)
        else:
            return NSTTypes(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def hasContent_(self):
        if (

        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='NSTTypes', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('NSTTypes')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None:
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='NSTTypes')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='NSTTypes', pretty_print=pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='NSTTypes'):
        pass
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='NSTTypes', fromsubclass_=False, pretty_print=True):
        pass
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        pass
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        pass
# end class NSTTypes


class Works(GeneratedsSuper):
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, Works)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if Works.subclass:
            return Works.subclass(*args_, **kwargs_)
        else:
            return Works(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def hasContent_(self):
        if (

        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='Works', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('Works')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None:
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='Works')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='Works', pretty_print=pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='Works'):
        pass
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='Works', fromsubclass_=False, pretty_print=True):
        pass
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        pass
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        pass
# end class Works


class CADParamsType(GeneratedsSuper):
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, Lookup=None, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = None
        if Lookup is None:
            self.Lookup = []
        else:
            self.Lookup = Lookup
        self.Lookup_nsprefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, CADParamsType)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if CADParamsType.subclass:
            return CADParamsType.subclass(*args_, **kwargs_)
        else:
            return CADParamsType(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def get_Lookup(self):
        return self.Lookup
    def set_Lookup(self, Lookup):
        self.Lookup = Lookup
    def add_Lookup(self, value):
        self.Lookup.append(value)
    def insert_Lookup_at(self, index, value):
        self.Lookup.insert(index, value)
    def replace_Lookup_at(self, index, value):
        self.Lookup[index] = value
    def hasContent_(self):
        if (
            self.Lookup
        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='CADParamsType', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('CADParamsType')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None:
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='CADParamsType')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='CADParamsType', pretty_print=pretty_print)
            showIndent(outfile, level, pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='CADParamsType'):
        pass
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='CADParamsType', fromsubclass_=False, pretty_print=True):
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        for Lookup_ in self.Lookup:
            namespaceprefix_ = self.Lookup_nsprefix_ + ':' if (UseCapturedNS_ and self.Lookup_nsprefix_) else ''
            Lookup_.export(outfile, level, namespaceprefix_, namespacedef_='', name_='Lookup', pretty_print=pretty_print)
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        pass
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        if nodeName_ == 'Lookup':
            obj_ = LookupType.factory(parent_object_=self)
            obj_.build(child_, gds_collector_=gds_collector_)
            self.Lookup.append(obj_)
            obj_.original_tagname_ = 'Lookup'
# end class CADParamsType


class LookupType(GeneratedsSuper):
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, Key=None, KeyName=None, Value=None, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = None
        self.Key = Key
        self.Key_nsprefix_ = None
        self.KeyName = KeyName
        self.KeyName_nsprefix_ = None
        self.Value = Value
        self.Value_nsprefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, LookupType)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if LookupType.subclass:
            return LookupType.subclass(*args_, **kwargs_)
        else:
            return LookupType(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def get_Key(self):
        return self.Key
    def set_Key(self, Key):
        self.Key = Key
    def get_KeyName(self):
        return self.KeyName
    def set_KeyName(self, KeyName):
        self.KeyName = KeyName
    def get_Value(self):
        return self.Value
    def set_Value(self, Value):
        self.Value = Value
    def hasContent_(self):
        if (
            self.Key is not None or
            self.KeyName is not None or
            self.Value is not None
        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='LookupType', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('LookupType')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None:
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='LookupType')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='LookupType', pretty_print=pretty_print)
            showIndent(outfile, level, pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='LookupType'):
        pass
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='LookupType', fromsubclass_=False, pretty_print=True):
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.Key is not None:
            namespaceprefix_ = self.Key_nsprefix_ + ':' if (UseCapturedNS_ and self.Key_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sKey>%s</%sKey>%s' % (namespaceprefix_ , self.gds_encode(self.gds_format_string(quote_xml(self.Key), input_name='Key')), namespaceprefix_ , eol_))
        if self.KeyName is not None:
            namespaceprefix_ = self.KeyName_nsprefix_ + ':' if (UseCapturedNS_ and self.KeyName_nsprefix_) else ''
            showIndent(outfile, level, pretty_print)
            outfile.write('<%sKeyName>%s</%sKeyName>%s' % (namespaceprefix_ , self.gds_encode(self.gds_format_string(quote_xml(self.KeyName), input_name='KeyName')), namespaceprefix_ , eol_))
        if self.Value is not None:
            namespaceprefix_ = self.Value_nsprefix_ + ':' if (UseCapturedNS_ and self.Value_nsprefix_) else ''
            self.Value.export(outfile, level, namespaceprefix_, namespacedef_='', name_='Value', pretty_print=pretty_print)
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        pass
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        if nodeName_ == 'Key':
            value_ = child_.text
            value_ = self.gds_parse_string(value_, node, 'Key')
            value_ = self.gds_validate_string(value_, node, 'Key')
            self.Key = value_
            self.Key_nsprefix_ = child_.prefix
        elif nodeName_ == 'KeyName':
            value_ = child_.text
            value_ = self.gds_parse_string(value_, node, 'KeyName')
            value_ = self.gds_validate_string(value_, node, 'KeyName')
            self.KeyName = value_
            self.KeyName_nsprefix_ = child_.prefix
        elif nodeName_ == 'Value':
            obj_ = ValueType.factory(parent_object_=self)
            obj_.build(child_, gds_collector_=gds_collector_)
            self.Value = obj_
            obj_.original_tagname_ = 'Value'
# end class LookupType


class ValueType(GeneratedsSuper):
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, xsi_type=None, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = None
        self.xsi_type = _cast(None, xsi_type)
        self.xsi_type_nsprefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, ValueType)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if ValueType.subclass:
            return ValueType.subclass(*args_, **kwargs_)
        else:
            return ValueType(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def get_xsi_type(self):
        return self.xsi_type
    def set_xsi_type(self, xsi_type):
        self.xsi_type = xsi_type
    def hasContent_(self):
        if (

        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='ValueType', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('ValueType')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None:
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='ValueType')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='ValueType', pretty_print=pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='ValueType'):
        if self.xsi_type is not None and 'xsi_type' not in already_processed:
            already_processed.add('xsi_type')
            outfile.write(' xsi:type=%s' % (self.gds_encode(self.gds_format_string(quote_attrib(self.xsi_type), input_name='xsi:type')), ))
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='ValueType', fromsubclass_=False, pretty_print=True):
        pass
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        value = find_attr_value_('xsi:type', node)
        if value is not None and 'xsi:type' not in already_processed:
            already_processed.add('xsi:type')
            self.xsi_type = value
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        pass
# end class ValueType


class LastChangeDateType(GeneratedsSuper):
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, xsi_nil=None, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = None
        self.xsi_nil = _cast(None, xsi_nil)
        self.xsi_nil_nsprefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, LastChangeDateType)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if LastChangeDateType.subclass:
            return LastChangeDateType.subclass(*args_, **kwargs_)
        else:
            return LastChangeDateType(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def get_xsi_nil(self):
        return self.xsi_nil
    def set_xsi_nil(self, xsi_nil):
        self.xsi_nil = xsi_nil
    def hasContent_(self):
        if (

        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='LastChangeDateType', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('LastChangeDateType')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None:
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='LastChangeDateType')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='LastChangeDateType', pretty_print=pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='LastChangeDateType'):
        if self.xsi_nil is not None and 'xsi_nil' not in already_processed:
            already_processed.add('xsi_nil')
            outfile.write(' xsi:nil=%s' % (self.gds_encode(self.gds_format_string(quote_attrib(self.xsi_nil), input_name='xsi:nil')), ))
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='', name_='LastChangeDateType', fromsubclass_=False, pretty_print=True):
        pass
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        value = find_attr_value_('xsi:nil', node)
        if value is not None and 'xsi:nil' not in already_processed:
            already_processed.add('xsi:nil')
            self.xsi_nil = value
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        pass
# end class LastChangeDateType


GDSClassesMapping = {
}


USAGE_TEXT = """
Usage: python <Parser>.py [ -s ] <in_xml_file>
"""


def usage():
    print(USAGE_TEXT)
    sys.exit(1)


def get_root_tag(node):
    tag = Tag_pattern_.match(node.tag).groups()[-1]
    rootClass = GDSClassesMapping.get(tag)
    if rootClass is None:
        rootClass = globals().get(tag)
    return tag, rootClass


def get_required_ns_prefix_defs(rootNode):
    '''Get all name space prefix definitions required in this XML doc.
    Return a dictionary of definitions and a char string of definitions.
    '''
    nsmap = {
        prefix: uri
        for node in rootNode.iter()
        for (prefix, uri) in node.nsmap.items()
        if prefix is not None
    }
    namespacedefs = ' '.join([
        'xmlns:{}="{}"'.format(prefix, uri)
        for prefix, uri in nsmap.items()
    ])
    return nsmap, namespacedefs


def parse(inFileName, silence=False, print_warnings=True):
    global CapturedNsmap_
    gds_collector = GdsCollector_()
    parser = None
    doc = parsexml_(inFileName, parser)
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'CalculationDto'
        rootClass = CalculationDto
    rootObj = rootClass.factory()
    rootObj.build(rootNode, gds_collector_=gds_collector)
    CapturedNsmap_, namespacedefs = get_required_ns_prefix_defs(rootNode)
    if not SaveElementTreeNode:
        doc = None
        rootNode = None
    if not silence:
        sys.stdout.write('<?xml version="1.0" ?>\n')
        rootObj.export(
            sys.stdout, 0, name_=rootTag,
            namespacedef_=namespacedefs,
            pretty_print=True)
    if print_warnings and len(gds_collector.get_messages()) > 0:
        separator = ('-' * 50) + '\n'
        sys.stderr.write(separator)
        sys.stderr.write('----- Warnings -- count: {} -----\n'.format(
            len(gds_collector.get_messages()), ))
        gds_collector.write_messages(sys.stderr)
        sys.stderr.write(separator)
    return rootObj


def parseEtree(inFileName, silence=False, print_warnings=True):
    parser = None
    doc = parsexml_(inFileName, parser)
    gds_collector = GdsCollector_()
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'CalculationDto'
        rootClass = CalculationDto
    rootObj = rootClass.factory()
    rootObj.build(rootNode, gds_collector_=gds_collector)
    # Enable Python to collect the space used by the DOM.
    mapping = {}
    rootElement = rootObj.to_etree(None, name_=rootTag, mapping_=mapping)
    reverse_mapping = rootObj.gds_reverse_node_mapping(mapping)
    if not SaveElementTreeNode:
        doc = None
        rootNode = None
    if not silence:
        content = etree_.tostring(
            rootElement, pretty_print=True,
            xml_declaration=True, encoding="utf-8")
        sys.stdout.write(str(content))
        sys.stdout.write('\n')
    if print_warnings and len(gds_collector.get_messages()) > 0:
        separator = ('-' * 50) + '\n'
        sys.stderr.write(separator)
        sys.stderr.write('----- Warnings -- count: {} -----\n'.format(
            len(gds_collector.get_messages()), ))
        gds_collector.write_messages(sys.stderr)
        sys.stderr.write(separator)
    return rootObj, rootElement, mapping, reverse_mapping


def parseString(inString, silence=False, print_warnings=True):
    '''Разбор строки, создание дерева объектов и его экспорт.

    Arguments:
    - inString -- A string.  Этот XML-фрагмент не должен
    начинаться с XML-объявления, содержащего кодировку.
    - silence -- A boolean.  Если значение False, экспортируйте объект.
    Returns -- Корневой объект в дереве.
    '''
    parser = None
    rootNode= parsexmlstring_(inString, parser)
    gds_collector = GdsCollector_()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'CalculationDto'
        rootClass = CalculationDto
    rootObj = rootClass.factory()
    rootObj.build(rootNode, gds_collector_=gds_collector)
    if not SaveElementTreeNode:
        rootNode = None
    if not silence:
        sys.stdout.write('<?xml version="1.0" ?>\n')
        rootObj.export(
            sys.stdout, 0, name_=rootTag,
            namespacedef_='')
    if print_warnings and len(gds_collector.get_messages()) > 0:
        separator = ('-' * 50) + '\n'
        sys.stderr.write(separator)
        sys.stderr.write('----- Warnings -- count: {} -----\n'.format(
            len(gds_collector.get_messages()), ))
        gds_collector.write_messages(sys.stderr)
        sys.stderr.write(separator)
    return rootObj


def parseLiteral(inFileName, silence=False, print_warnings=True):
    parser = None
    doc = parsexml_(inFileName, parser)
    gds_collector = GdsCollector_()
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'CalculationDto'
        rootClass = CalculationDto
    rootObj = rootClass.factory()
    rootObj.build(rootNode, gds_collector_=gds_collector)
    # Enable Python to collect the space used by the DOM.
    if not SaveElementTreeNode:
        doc = None
        rootNode = None
    if not silence:
        sys.stdout.write('#from generator import *\n\n')
        sys.stdout.write('import generator as model_\n\n')
        sys.stdout.write('rootObj = model_.rootClass(\n')
        rootObj.exportLiteral(sys.stdout, 0, name_=rootTag)
        sys.stdout.write(')\n')
    if print_warnings and len(gds_collector.get_messages()) > 0:
        separator = ('-' * 50) + '\n'
        sys.stderr.write(separator)
        sys.stderr.write('----- Warnings -- count: {} -----\n'.format(
            len(gds_collector.get_messages()), ))
        gds_collector.write_messages(sys.stderr)
        sys.stderr.write(separator)
    return rootObj


def main():
    args = sys.argv[1:]
    if len(args) == 1:
        parse(args[0])
    else:
        usage()


if __name__ == '__main__':
    #import pdb; pdb.set_trace()
    main()

RenameMappings_ = {
}

__all__ = [
    "CADParamsType",
    "CalculationDto",
    "CalculationItemDtoType",
    "ItemsType",
    "LastChangeDateType",
    "LookupType",
    "ValueType"
]
